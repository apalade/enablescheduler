package ie.scss.tcd.enable.simulator.util;

import java.util.Random;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements utilities that are used across the system
 * 
 */
public class GeoUtil {

    public static double getDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    }
    
    /**
     * Generate a number of location with a given seed
     * 
     * @param number The number of locations to be generated
     * @param xRange Is the range of the X axis (e.g., 0 to 400)
     * @param yRange Is the range of the Y axis (e.g., 0 to 400)
     * @param seed The seed used in the random number generator to generate the same numbers
     * @return An array of Random locations
     */
    public static double[][] generateRandomLocations(int number, double xRange, double yRange, int seed) {
        
        double[][] locations = new double[number][2];
        
        double start = 10; // we could use also the origin (e.g., 0) 
        
        for (int i = 0; i < number; i++) {
            double randomX = new Random().nextDouble();
            double randomY = new Random().nextDouble();
            double resultX = start + (randomX * (xRange - start));
            double resultY = start + (randomY * (xRange - start));
            locations[i] = new double[]{resultY, resultX};
        }
        
        return locations;
    }
    
    public static void main(String[] args) {
        
        // Testing the getDistance() method
//        double userX = 10;
//        double userY = 10;
//        
//        double[] x = {10, 20, 30, 15};
//        double[] y = {14, 10, 5, 10};
//        
//        double total = 0;
//        
//        for (int i = 0; i < x.length; i++) {
//            total += GeoUtil.getDistance(userX, userY, x[i], y[i]);
//        }
//        
//        System.out.println(total);
        
        
        // Testing the generateRandomLocations() method
        
        double[][] locations = generateRandomLocations(10, 400, 400, 10);
        
        for (int i = 0; i < locations.length; i++) {
            System.out.println(locations[i][0] + " - " + locations[i][1]);
        }
    }
}
