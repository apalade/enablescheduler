package ie.scss.tcd.enable.simulator.util;

import java.util.Arrays;
import java.util.Collections;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements utilities that are used across the system
 * 
 */
public class DataUtil {
    
    public static double getMax(double[] numbers) {
        return Collections.max(Arrays.asList(ArrayUtils.toObject(numbers)));
    }
    
    public static double getMin(double[] numbers) {
        return Collections.min(Arrays.asList(ArrayUtils.toObject(numbers)));
    }
    
    public static double[] normalise(double[] data) {
        double min = DataUtil.getMin(data);
        double max = DataUtil.getMax(data);
        double[] utility = new double[data.length];
        
        for (int i = 0; i < data.length; i++) {
            if (max == min) {
                utility[i] = 1.0;
            }
            else {
                utility[i] = (data[i] - min) / (max - min);
            }
        }
        
        return utility;
    }
    
    public static void main(String[] args) {
        double[] numbers = new double[5];
        
        numbers[0] = 1;
        numbers[1] = 5;
        numbers[2] = 6;
        numbers[3] = 3;
        numbers[4] = 4;
        
        double[] utility = DataUtil.normalise(numbers);
        
        for (int i = 0; i < utility.length; i++) {
            System.out.println(utility[i]);
        }
    }
}
