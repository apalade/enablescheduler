package ie.scss.tcd.enable.simulator.util;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements utilities that are used across the system 
 * 
 */
public class SolverUtil {

    public int[] generateIndices(int n) {

        Model model = new Model("indices ");

        IntVar[] numbers = model.intVarArray(n, 1, 5);
        model.sum(numbers, "+", 20).post();
        model.getSolver().solve();

        int[] indices = new int[n];

        for (int i = 0; i < numbers.length; i++) {
            indices[i] = numbers[i].getValue();
        }

        return indices;
    }

}
