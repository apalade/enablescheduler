package ie.scss.tcd.enable.simulator.scheduler;

import ie.scss.tcd.enable.simulator.model.Placement;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public interface Scheduler {
    
    public Placement schedule();
}
