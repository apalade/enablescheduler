package ie.scss.tcd.enable.simulator.scheduler.aco;

import ie.scss.tcd.enable.simulator.model.Location;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents the memory of an ANT, used to deposite the pheromones in the backward propagation process.
 * 
 */
public class ANTMemory {
    
    private int nodeID;
    private double latency;
    private double cpuUtil;
    private double ramUtil;
    private Location location;
    
    public ANTMemory(int nodeID, double latency, double cpuUtil, double ramUtil) {
        this(nodeID, latency, cpuUtil, ramUtil, null);
    }
    
    public ANTMemory(int nodeID, double latency, double cpuUtil, double ramUtil, Location location) {
        this.nodeID = nodeID;
        this.latency = latency;
        this.cpuUtil = cpuUtil;
        this.ramUtil = ramUtil;
        this.location = location;
    }

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public double getLatency() {
        return latency;
    }

    public void setLatency(double latency) {
        this.latency = latency;
    }

    public double getCpuUtil() {
        return cpuUtil;
    }

    public void setCpuUtil(double cpuUtil) {
        this.cpuUtil = cpuUtil;
    }

    public double getRamUtil() {
        return ramUtil;
    }

    public void setRamUtil(double ramUtil) {
        this.ramUtil = ramUtil;
    }
    
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
