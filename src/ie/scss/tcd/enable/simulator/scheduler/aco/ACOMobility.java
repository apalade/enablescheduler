package ie.scss.tcd.enable.simulator.scheduler.aco;


import ie.scss.tcd.enable.simulator.model.MECServer;
import ie.scss.tcd.enable.simulator.model.Placement;
import ie.scss.tcd.enable.simulator.model.User;
import ie.scss.tcd.enable.simulator.scheduler.Scheduler;
import ie.scss.tcd.enable.simulator.util.DataUtil;
import ie.scss.tcd.enable.simulator.util.GeoUtil;


/**
 * ACO implementation that uses the distance to select the optimal solution
 * 
 * processSolution() method is where the selection is performed
 * 
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class ACOMobility implements Scheduler {
    
    // Total number of nodes
    private int nodes;
    
    // Total number of containers
    private int containers;
    
    // Total ants number
    private int antsNumber;
    
    // Ants that will explore
    private ANT[] ants;
    
    // Total number of iterations to be performed
    private int iterations;
    
    // If alfa is 0, then the closest nodes are more likely 
    // to be selected this corresponds to a classical 
    // stochastic greedy algorithm (with multiple starting 
    // points since ants are initially randomly distributed 
    // on the cities).
    // private double alfa = 1.0;
    private double alfa;
    
    // If beta is 0, then only pheromone amplification is at 
    // work this method will lead to a rapid stagnation situation
    // with the corresponding generation of tours which, in general, 
    // are strongly suboptimal 
    // private double beta = 3.0;
    private double beta;
    
    // Global evaporation rate
    // private double ro = 0.5;
    private double ro;
    
    // Initial pheromone level
    // private double tau0 = 1.0;
    private double tau0;
    
    // This is the amount of pheromone deposit used for each branch
    private double deltaTau;
    
    // Stores the pheromone levels
    private double[][] pheromone;
    
    // Stores the heuristic values
    private double[][] heuristic;
    
    // Stores the latency between nodes
    private double[][] networkLatency;
    
    // Stores the CPU utilisation
    //private double[] cpuUtil;
    
    // Stores the RAM utilisation
    //private double[] ramUtil;
    
    private MECServer[] servers;
    
    private User user;
    
    public ACOMobility(int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            int antsNumber, int iterations, 
            double alfa, double beta, double ro, double tau0, double deltaTau,
            double[][] networkLatency, 
            MECServer[] servers) {
        
        this.servers = servers;
        this.nodes = servers.length;
        this.containers = containers;
        
        this.antsNumber = antsNumber;
        this.ants = new ANT[antsNumber];
        this.iterations = iterations;
        
        this.alfa = alfa;
        this.beta = beta;
        this.ro = ro;
        this.tau0 = tau0;
        this.deltaTau = deltaTau;
        
        this.networkLatency = networkLatency;
        
        this.pheromone = new double[nodes][containers];
        this.heuristic = new double[nodes][containers];
 
        
        initAnts();
        initPheromones();
    }
    
    public void initPheromones() {
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < containers; j++) {
                pheromone[i][j] = tau0;
            }
        }
    }
    
    public void initAnts() {
        for (int i = 0; i < antsNumber; i++) {
            this.ants[i] = new ANT(containers);
        }
    }
    
    public double[][] getPheronome() {
        return pheromone;
    }

    public void setPheronome(double[][] pheronome) {
        this.pheromone = pheronome;
    }
    
    // Basic ACO methods
    public int[] acoSchedule() {
        
        // Asign initial node to each ant
//        Random rand = new Random();
//        for (int i = 0; i < antsNumber; i++) {
//            int r = Math.abs(rand.nextInt()) % nodes;
//            ants[i].addMemory(new Memory(r, 0, cpuUtil[r], ramUtil[r]));
//        }

        int[] placements = new int[containers];
    
        for (int iter = 0; iter < iterations; iter++) {

            for (int j = 0; j < containers; j++) {
                for (int k = 0; k < antsNumber; k++) {
                    
                    //computeHeuristic();
                    
                    forwardMovement(j, k);
                }
            }

            int optimalSolutionIndex = processSolutions();
            ants[optimalSolutionIndex].setSuperior(true);
            
            for (int m = 0; m < ants[optimalSolutionIndex].getMemories().length; m++) {
                placements[m] = ants[optimalSolutionIndex].getMemories()[m].getNodeID();
            }
        
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < ants[optimalSolutionIndex].getMemories().length; i++) {
                builder.append(ants[optimalSolutionIndex].getMemories()[i].getNodeID()).append(" ");
            }
            
            //System.out.println(builder.toString());
            
            globalEvaporation();
            
            // Build the solutions
            for (int j = 0; j < containers; j++) {
                for (int k = 0; k < antsNumber; k++) {
                    backwardMovement(j, k);
                }
            }
            
            for (int k = 0; k < antsNumber; k++) {
                ants[k].reset();
            }
            
            //printPhermone();
            
        }
        
        return placements;
    }
    
    public void forwardMovement(int cIndex, int aIndex) {
        
        double sumProb = 0.0;
        double[] selectionProb = new double[nodes];
        
        for (int i = 0; i < nodes; i++) {
            
            double niu;
        
            // DOUBLE CHECK THIS
            if (cIndex == 0) {
                niu = 1.0 / 0.01;
            }
            else {
                int curNode = ants[aIndex].getCurrentNode();
             
                if (networkLatency[curNode][i] > 0) {
                    niu = 1.0 / networkLatency[curNode][i];
                } else {
                    niu = 1.0 / 0.01;
                }
            }
            
            selectionProb[i] =  Math.pow(pheromone[i][cIndex], alfa) * Math.pow(niu, beta);
            sumProb += selectionProb[i];
        }
        
        double prob = Math.random() * sumProb;
        int j = 0; 
        double p = selectionProb[j];
        while(p < prob) {
            j++;
            p += selectionProb[j];
        }
        
        if (cIndex == 0) {
            
            ants[aIndex].addMemory(cIndex, 
                    new ANTMemory(j, networkLatency[j][j], servers[j].getCPUUtil(), 
                            servers[j].getRAMUtil(),  servers[j].getLocation()));
        }
        else {
            
            int cNode = ants[aIndex].getCurrentNode();
            
            ants[aIndex].addMemory(cIndex, 
                    new ANTMemory(j, networkLatency[cNode][j], servers[j].getCPUUtil(), 
                            servers[j].getRAMUtil(), servers[j].getLocation()));
        }
        ants[aIndex].setCurrentNode(j);
        
//        if (aIndex == 0) {
//            System.out.println(j);
//        }
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public void backwardMovement(int cIndex, int aIndex) {
        depositPheronome(cIndex, aIndex);
    }
    
    public int processSolutions() {
    
        double[] latencyValues = new double[antsNumber];
        double[] distanceValues = new double[antsNumber];
        
        for (int i = 0; i < antsNumber; i++) {
            
            double totalLatency = 0;
            double totalDistance = 0;
            
            for (int j = 0; j < ants[i].getMemories().length; j++) {
                totalLatency += ants[i].getMemories()[j].getLatency();
                totalDistance += GeoUtil.getDistance(
                    user.getLocation().getX(), 
                    user.getLocation().getY(), 
                    ants[i].getMemories()[j].getLocation().getX(), 
                    ants[i].getMemories()[j].getLocation().getY());
            }
            
            latencyValues[i] = totalLatency;
            distanceValues[i] = totalDistance;
        }
        
        double[] normLatency = DataUtil.normalise(latencyValues);
        double[] normDistance = DataUtil.normalise(distanceValues);
        
        double minUtil = -1;
        int minUtilIndex = -1;
        
        // We are minimising the utility since we are minimising both latency and distance
        for (int i = 0; i < normLatency.length; i++) {
            
            double tempUtil = normDistance[i] * 0.5 + normLatency[i] * 0.5;
            
            if (minUtil == -1) {
                minUtil = tempUtil;
                minUtilIndex = i;
            }
            else {
                if (tempUtil < minUtil) {
                    minUtil = tempUtil;
                    minUtilIndex = i;
                }
            }
        }
         
        return minUtilIndex;
    }
    
    public void depositPheronome(int cIndex, int aIndex) {
        
        if (ants[aIndex].isSuperior()) {
            int nodeID = ants[aIndex].getMemories()[cIndex].getNodeID();
            pheromone[nodeID][cIndex] = pheromone[nodeID][cIndex] + deltaTau;
        }
    }
    
    public void computeHeuristic() {
        double niu;
        
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < containers; j++) {
                
                // DOUBLE CHECK THIS
                if (networkLatency[i][j] > 0) {
                    niu = 1.0 / networkLatency[i][j];
                } else {
                    niu = 1.0 / 0.0001;
                }
                
                heuristic[i][j] = Math.pow(pheromone[i][j], alfa) * Math.pow(niu, beta);
            }
        }
    }
    
    // Decrease the pheromone level in the pheron
    public void globalEvaporation() {
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < containers; j++) {
                pheromone[i][j] = (1 - ro) * pheromone[i][j];
            }
        }
    }
    
    public void getAntLocation() {
        for (int i = 0; i < ants.length; i++) {
            System.out.println("Ant " + i + " ");
        }
    }
    
    private void printPhermone() {
        
        for (int i = 0; i < pheromone.length; i++) {
            for (int j = 0; j < pheromone[i].length; j++) {
                System.out.print(pheromone[i][j] + " ");
            }
            System.out.println();
        }
        
        System.out.println();
        System.out.println();
    }
    
    public Placement schedule() {

        int[] solution = acoSchedule();

        Placement placement = new Placement(solution);
        return placement;
    }
} 
