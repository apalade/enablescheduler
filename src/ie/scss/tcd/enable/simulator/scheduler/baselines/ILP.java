package ie.scss.tcd.enable.simulator.scheduler.baselines;

import ie.scss.tcd.enable.simulator.model.MECServer;
import ie.scss.tcd.enable.simulator.model.Placement;
import ie.scss.tcd.enable.simulator.scheduler.Scheduler;
import java.util.ArrayList;
import java.util.List;
import org.btrplace.model.DefaultModel;
import org.btrplace.model.Mapping;
import org.btrplace.model.Model;
import org.btrplace.model.Node;
import org.btrplace.model.VM;
import org.btrplace.model.constraint.Running;
import org.btrplace.model.constraint.SatConstraint;
import org.btrplace.model.view.ShareableResource;
import org.btrplace.plan.DependencyBasedPlanApplier;
import org.btrplace.plan.ReconfigurationPlan;
import org.btrplace.plan.TimeBasedPlanApplier;
import org.btrplace.scheduler.choco.ChocoScheduler;
import org.btrplace.scheduler.choco.DefaultChocoScheduler;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class ILP extends Heuristic implements Scheduler {

    public ILP(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency,
            MECServer[] servers) {

        super(containers, containersCPUReq, containersRAMReq,
                networkLatency, servers);
    }

    @Override
    public Placement schedule() {

        int containers = getContainers();
        MECServer[] servers = getServers();
        double[] containersCPUReq = getContainersCPUReq();
        double[] containersRAMReq = getContainersRAMReq();
        
        List<VM> vms = new ArrayList<>();
        Model model = new DefaultModel();
        Mapping map = model.getMapping();
        
        ShareableResource rcCPU = new ShareableResource("cpu");
        ShareableResource rcMem = new ShareableResource("mem");

        for (int i = 0; i < servers.length; i++) {
            Node n = model.newNode();
            int cpu = (int) servers[i].getAvailableCPU();
            int ram = (int) servers[i].getAvailableRAM();
            rcCPU.setCapacity(n, cpu <= 0 ? 1: cpu);
            rcMem.setCapacity(n, ram <= 0 ? 1: ram);
            map.addOnlineNode(n);
        }

        for (int i = 0; i < containers; i++) {
            VM v = model.newVM();
            
            rcCPU.setConsumption(v, (int) containersCPUReq[i]);
            rcMem.setConsumption(v, (int) containersRAMReq[i]);
            
            vms.add(v);
            map.addReadyVM(v);
        }

        //Attach the resources
        model.attach(rcCPU);
        model.attach(rcMem);
        
        List<SatConstraint> constraints = new ArrayList<>();
        
        for (int i = 0; i < containers; i++) {
            constraints.add(new Running(vms.get(i)));
        }
        
        ChocoScheduler ra = new DefaultChocoScheduler();
        ReconfigurationPlan plan = ra.solve(model, constraints);
        
         int[] placement = new int[containers];
        
        if (plan != null) {
            
            for (int i = 0; i < plan.getSize(); i++) {
                int nodeID = plan.getResult().getMapping().getVMLocation(vms.get(i)).id();
                placement[i] = nodeID;
            }
            
//            System.out.println("Time-based plan:");
//            System.out.println(new TimeBasedPlanApplier().toString(plan));
//            System.out.println("\nDependency based plan:");
//            System.out.println(new DependencyBasedPlanApplier().toString(plan));
        }        
        
        Placement solution = new Placement(placement);
        return solution;
    }

}
