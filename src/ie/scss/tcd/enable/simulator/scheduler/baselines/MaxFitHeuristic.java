package ie.scss.tcd.enable.simulator.scheduler.baselines;

import ie.scss.tcd.enable.simulator.model.MECServer;
import ie.scss.tcd.enable.simulator.model.Placement;
import ie.scss.tcd.enable.simulator.scheduler.Scheduler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the MaxFit baseline
 * 
 */
public class MaxFitHeuristic extends Heuristic implements Scheduler {

    public MaxFitHeuristic(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency,
            MECServer[] servers) {

        super(containers, containersCPUReq, containersRAMReq,
                networkLatency, servers);
    }

    public Placement schedule() {

        int containers = getContainers();
        double[] containersCPUReq = getContainersCPUReq();
        double[] containersRAMReq = getContainersRAMReq();
        double[][] networkLatency = getNetworkLatency();
        MECServer[] servers = getServers();

        List<MECServer> nodes = new ArrayList<>();

        for (int i = 0; i < servers.length; i++) {
            nodes.add(servers[i]);
        }
        
        // create the placement list
        int[] placements = new int[containers];

        for (int i = 0; i < containers; i++) {
            Collections.reverse(nodes);

            for (int j = 0; j < nodes.size(); j++) {
                if (nodes.get(j).canFit(containersCPUReq[i], containersRAMReq[i])) {
                    nodes.get(j).update(containersCPUReq[i], containersRAMReq[i]);
                    placements[i] = j;
                }
            }
        }

        Placement solution = new Placement(placements);
        return solution;
    }
}
