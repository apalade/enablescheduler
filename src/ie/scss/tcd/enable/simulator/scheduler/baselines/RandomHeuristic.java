package ie.scss.tcd.enable.simulator.scheduler.baselines;

import ie.scss.tcd.enable.simulator.model.MECServer;
import ie.scss.tcd.enable.simulator.model.Placement;
import ie.scss.tcd.enable.simulator.scheduler.Scheduler;
import java.util.Random;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the Random baseline
 * 
 */
public class RandomHeuristic extends Heuristic implements Scheduler {
    
    
    public RandomHeuristic(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency,
            MECServer[] servers) {

        super(containers, containersCPUReq, containersRAMReq,
                networkLatency, servers);
    }
    
    @Override
    public Placement schedule() {
        
        int containers = getContainers();
        MECServer[] servers = getServers();
        
        int[] placements = new int[containers];
        
        Random random = new Random();
        
        for (int i = 0; i < containers; i++) {
            placements[i] = random.nextInt(servers.length);
        }
        
        Placement solution = new Placement(placements);
        return solution;
    }
}
