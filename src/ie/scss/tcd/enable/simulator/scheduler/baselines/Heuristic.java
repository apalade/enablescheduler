package ie.scss.tcd.enable.simulator.scheduler.baselines;

import ie.scss.tcd.enable.simulator.model.MECServer;
import java.util.List;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Heuristic {
    
    private MECServer[] servers;
    private int containers;
    
    private double[] containersCPUReq;
    private double[] containersRAMReq;
    private double[][] networkLatency;
    
    public Heuristic (int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency, 
            MECServer[] servers) {
        
        this.containers = containers;
        this.containersCPUReq = containersCPUReq;
        this.containersRAMReq = containersRAMReq;
        this.networkLatency = networkLatency;
        this.servers = servers;
    }
    
    public int getContainers() {
        return containers;
    }

    public double[] getContainersCPUReq() {
        return containersCPUReq;
    }

    public double[] getContainersRAMReq() {
        return containersRAMReq;
    }

    public double[][] getNetworkLatency() {
        return networkLatency;
    }

    public MECServer[] getServers() {
        return servers;
    }
}
