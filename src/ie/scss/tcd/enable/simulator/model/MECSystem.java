package ie.scss.tcd.enable.simulator.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a MEC system in the simulation
 * 
 */
public class MECSystem {

    private List<MECServer> servers;
    private int id;
    
    public MECSystem () {
        this.servers = new ArrayList<>();
    }
    
    public MECSystem (List<MECServer> servers) {
        this.servers = servers;
    }
    
    public int getSize() {
        return servers.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void addMECServer(MECServer server) {
        servers.add(server);
    }
}
