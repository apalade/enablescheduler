package ie.scss.tcd.enable.simulator.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a federated MEC system in the simulation
 * 
 */
public class MECFederated {
    
    private List<MECSystem> mecSystems;
    
    public MECFederated() {
        this.mecSystems = new ArrayList<>();
    }
    
    /**
     * @return An integer with the number of MEC systems
     */
    public int getSize() {
        return mecSystems.size();
    }
    
    public void addMECSystem(MECSystem mecSystem) {
        mecSystems.add(mecSystem);
    }
}
