package ie.scss.tcd.enable.simulator.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a MEC server in the system in the simulation
 * 
 */
public class MECServer implements Comparable<MECServer> {
    
    private int id;
    
    private double cpuReserved;
    private double cpuTotal;
    private double ramReserved;
    private double ramTotal;
    
    private Location location;
    
    public MECServer(int id, double cpuReserved, double cpuTotal, 
            double ramReserved, double ramTotal) {
        this(id, cpuReserved, cpuTotal, ramReserved, ramTotal, null);
    }
    
    public MECServer(int id, double cpuReserved, double cpuTotal, 
            double ramReserved, double ramTotal, Location location) {
        
        this.id = id;
        this.cpuReserved = cpuReserved;
        this.cpuTotal = cpuTotal;
        this.ramReserved = ramReserved;
        this.ramTotal = ramTotal;
        this.location = location;
    }
    
    public int getID() {
        return id;
    }
    
    public boolean canFit(double cpu, double ram) {
        return (cpu + cpuReserved) <= cpuTotal && (ram + ramReserved) <= ramTotal;
    }
    
    public void update(double cpu, double ram) {
        cpuReserved += cpu;
        ramReserved += ram;
    }
    
    public double getCPUUtil() {
        return cpuReserved / cpuTotal;
    }
    
    public double getRAMUtil() {
        return ramReserved / ramTotal;
    }
    
    public double getAvailableCPU() {
        return cpuTotal - cpuReserved;
    }
    
    public double getAvailableRAM() {
        return ramTotal - ramReserved;
    }

    public double getCpuReserved() {
        return cpuReserved;
    }

    public double getCpuTotal() {
        return cpuTotal;
    }

    public double getRamReserved() {
        return ramReserved;
    }

    public double getRamTotal() {
        return ramTotal;
    }
    
    public Location getLocation() {
        return location;
    }
    
    @Override
    public int compareTo(MECServer arg0) {
        
        if (this.getCPUUtil() < arg0.getCPUUtil()) {
            return -1;
        }
        else if (this.getCPUUtil() == arg0.getCPUUtil()) {
            
            if (this.getRAMUtil() < arg0.getRAMUtil()) {
                return -1;
            }
            else if (this.getRAMUtil() == arg0.getRAMUtil()) {
                return 0;
            }
            else {
                return 1;
            }
        }
        else {
            return 1;
        }
    }
}
