package ie.scss.tcd.enable.simulator.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class presents an User in the simulation
 * 
 */
public class User {
    
    private Location location;

    public User(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
