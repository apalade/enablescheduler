package ie.scss.tcd.enable.simulator.model;

import java.util.Map;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a placement decision from the simulation
 * 
 */
public class Placement {
    
    // index represents the id of the container
    // the value at a certain index represents the node where to place that container
    private int[] placement;
    
    public Placement(int[] placement) {
        this.placement = placement;
    }
    
    public int[] getPlacement() {
        return placement;
    }
}
