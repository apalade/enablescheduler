package ie.scss.tcd.enable.simulator.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represent a task to be placed in the simulation
 * 
 */
public class Task {
    
    public double cpuRequirement;
    public double ramRequirement;

}
