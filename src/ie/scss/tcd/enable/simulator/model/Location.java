package ie.scss.tcd.enable.simulator.model;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents the location of a node or request in the simulation 
 * 
 */
public class Location {

    private double x;
    private double y;

    public Location(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
