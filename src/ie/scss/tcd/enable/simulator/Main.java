package ie.scss.tcd.enable.simulator;

import ie.scss.tcd.enable.simulator.model.Location;
import ie.scss.tcd.enable.simulator.model.MECServer;
import ie.scss.tcd.enable.simulator.model.Placement;
import ie.scss.tcd.enable.simulator.model.User;
import ie.scss.tcd.enable.simulator.scheduler.aco.ACO;
import ie.scss.tcd.enable.simulator.scheduler.aco.ACOMobility;
import ie.scss.tcd.enable.simulator.scheduler.baselines.BestFitHeuristic;
import ie.scss.tcd.enable.simulator.scheduler.baselines.ILP;
import ie.scss.tcd.enable.simulator.scheduler.baselines.MaxFitHeuristic;
import ie.scss.tcd.enable.simulator.scheduler.baselines.MultiOpt;
import ie.scss.tcd.enable.simulator.scheduler.baselines.RandomHeuristic;
import ie.scss.tcd.enable.simulator.util.GeoUtil;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class executes the simulation experiments
 * 
 */
public class Main {

    private static long SEED = 200;

    public static double aggregateNetworkLatency(int[] placements, double[][] networkLatency) {

        double sum = 0;

        for (int i = 1; i < placements.length; i++) {
            sum += networkLatency[placements[i - 1]][placements[i]];
        }

        return sum;
    }

    public static double getResourceUtilisation(int[] placements,
            double[] containerCPUReq, double[] containerRAMReq, MECServer[] servers) {

        double[] cpuUtil = new double[servers.length];
        double[] ramUtil = new double[servers.length];

        for (int i = 0; i < servers.length; i++) {
            cpuUtil[i] = servers[i].getCPUUtil();
            ramUtil[i] = servers[i].getRAMUtil();
        }

        for (int i = 0; i < placements.length; i++) {

            double cpuReq = servers[placements[i]].getCpuReserved();
            double cpuTotal = servers[placements[i]].getCpuTotal();
            cpuUtil[placements[i]] = (cpuReq + containerCPUReq[i]) / cpuTotal;

            double ramReq = servers[placements[i]].getRamReserved();
            double ramTotal = servers[placements[i]].getRamTotal();
            ramUtil[placements[i]] = (ramReq + containerRAMReq[i]) / ramTotal;
        }

        DescriptiveStatistics cpuStats = new DescriptiveStatistics(cpuUtil);
        DescriptiveStatistics ramStats = new DescriptiveStatistics(cpuUtil);

        return Math.sqrt(
                0.5 * Math.pow(cpuStats.getStandardDeviation(), 2)
                + 0.5 * Math.pow(ramStats.getStandardDeviation(), 2));
    }

    public static double generateInRange(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    public static double[][] generateLatency(int numberOfServers) {
        double[][] latency = new double[numberOfServers][numberOfServers];

        for (int i = 0; i < latency.length; i++) {
            for (int j = 0; j < latency[i].length; j++) {

                if (i == j) {
                    latency[i][j] = 0;
                } else {
                    latency[i][j] = generateInRange(0.5, 150);
                }
            }
        }

        return latency;
    }

    public static double[] generateContainerCPUReq(int numberOfContainers) {

        double[] cpuReq = new double[numberOfContainers];
        for (int i = 0; i < numberOfContainers; i++) {
            cpuReq[i] = generateInRange(0.01, 3);
        }
        return cpuReq;
    }

    public static double[] generateContainerRAMReq(int numberOfContainers) {

        double[] ramReq = new double[numberOfContainers];
        for (int i = 0; i < numberOfContainers; i++) {
            ramReq[i] = generateInRange(0.01, 3);
        }
        return ramReq;
    }

    public static MECServer[] generateMECServers(int number, int numberOfServers) {

        MECServer[] servers = new MECServer[number * numberOfServers];

        for (int i = 0; i < number * numberOfServers; i++) {
            double cpuReserved = generateInRange(0, 15);
            double cpuTotal = generateInRange(cpuReserved, 20);
            double ramReserved = generateInRange(0, 15);
            double ramTotal = generateInRange(ramReserved, 24);
            servers[i] = new MECServer(i, cpuReserved, cpuTotal, ramReserved, ramTotal);
        }

        return servers;
    }

    public static void print2D(double mat[][]) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(mat[i][j] + " ");
            }
            System.out.println();
        }
    }

    class Solution {

        private double networkLatency;
        private double executionTime;
        private double resourceUtilisation; // st deviation

        public Solution(double networkLatency, double executionTime, double resourceUtilisation) {
            this.networkLatency = networkLatency;
            this.executionTime = executionTime;
            this.resourceUtilisation = resourceUtilisation;
        }

        public double getResourceUtilisation() {
            return resourceUtilisation;
        }

        public double getNetworkLatency() {
            return networkLatency;
        }

        public double getExecutionTime() {
            return executionTime;
        }

        public String toString() {
            return networkLatency + ", " + executionTime + ", " + resourceUtilisation;
        }
    }

    /**
     * ACO implementation for latency
     * 
     * @param containers Number of containers (one container per function/service)
     * @param containersCPUReq CPU Requirements for each function/service
     * @param containersRAMReq RAM Requirements for each function/service
     * @param networkLatencies Network latency between network nodes
     * @param servers Available MEC servers
     * @return 
     */
    public Solution runACO(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {

        int antsNumber = 10;
        int iterations = 20000;

        double alfa = 0.002;
        double beta = 0.0001;
        double ro = 0.005;
        double tau0 = 1.0;
        double deltaTau = 0.001;

        //long start = System.currentTimeMillis();
        long start = System.nanoTime();
        ACO aco = new ACO(
                containers, containersCPUReq, containersRAMReq,
                antsNumber, iterations,
                alfa, beta, ro, tau0, deltaTau,
                networkLatencies, servers
        );

        Placement solution = aco.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }
    
    /**
     * ACO implementation for distance
     * 
     * @param containers Number of containers (one container per function/service)
     * @param containersCPUReq CPU Requirements for each function/service
     * @param containersRAMReq RAM Requirements for each function/service
     * @param networkLatencies Network latency between network nodes
     * @param servers Available MEC servers
     * @param user The user associated with this request
     * @return 
     */
    public Solution runACOMobility(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers, User user) {

        int antsNumber = 10;
        int iterations = 20000;

        double alfa = 0.002;
        double beta = 0.0001;
        double ro = 0.005;
        double tau0 = 1.0;
        double deltaTau = 0.001;

        //long start = System.currentTimeMillis();
        long start = System.nanoTime();
        ACOMobility aco = new ACOMobility(
                containers, containersCPUReq, containersRAMReq,
                antsNumber, iterations,
                alfa, beta, ro, tau0, deltaTau,
                networkLatencies, servers
        );
        
        aco.setUser(user);

        Placement solution = aco.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public Solution runBestFit(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {

        //long start = System.currentTimeMillis();
        long start = System.nanoTime();

        BestFitHeuristic bestFitHeuristic = new BestFitHeuristic(
                containers, containersCPUReq, containersRAMReq,
                networkLatencies, servers
        );
        Placement solution = bestFitHeuristic.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public Solution runMaxFit(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {
        //long start = System.currentTimeMillis();
        long start = System.nanoTime();

        MaxFitHeuristic maxFitHeuristic = new MaxFitHeuristic(
                containers, containersCPUReq, containersRAMReq,
                networkLatencies, servers
        );
        Placement solution = maxFitHeuristic.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public Solution runMultiOpt(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {
        //long start = System.currentTimeMillis();
        long start = System.nanoTime();

        MultiOpt multiOpt = new MultiOpt(
                containers, containersCPUReq, containersRAMReq,
                networkLatencies, servers
        );
        Placement solution = multiOpt.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public Solution runILP(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {
        //long start = System.currentTimeMillis();
        long start = System.nanoTime();

        ILP ilp = new ILP(
                containers, containersCPUReq, containersRAMReq,
                networkLatencies, servers
        );
        Placement solution = ilp.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public Solution runRandom(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatencies,
            MECServer[] servers) {
        //long start = System.currentTimeMillis();
        long start = System.nanoTime();

        RandomHeuristic randomHeuristic = new RandomHeuristic(
                containers, containersCPUReq, containersRAMReq,
                networkLatencies, servers
        );
        Placement solution = randomHeuristic.schedule();

        //double executionTime = System.currentTimeMillis() - start;
        double executionTime = System.nanoTime() - start;
        double networkLatency = aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
        double resourceUtilisation = getResourceUtilisation(
                solution.getPlacement(), containersCPUReq, containersRAMReq, servers);

        return new Solution(networkLatency, executionTime, resourceUtilisation);
    }

    public static void main(String[] args) throws IOException {

        int experimentRepetition = 50;
        
        int containers = 5;

        for (int mecSystem = 4; mecSystem <= 5; mecSystem++) {
            
            String fileName = "output/output_" + containers + "_containers_" + mecSystem + "_MECSystems_" + "n_MECServersPerSystem.csv";               
            FileWriter csvWriter = new FileWriter(fileName);
            
            for (int mecServerPerSystem = 2; mecServerPerSystem <= 5; mecServerPerSystem++) {


                double[][] resNetworkLatency = new double[7][experimentRepetition];
                double[][] resResourceUtil = new double[7][experimentRepetition];
                double[][] resExecutionTime = new double[7][experimentRepetition];
                
                for (int iteration = 0; iteration < experimentRepetition; iteration++) {

                    int totalNodes = mecSystem * mecServerPerSystem;

                    double[][] networkLatencies = generateLatency(totalNodes);

                    double[] containersCPUReq = generateContainerCPUReq(containers);
                    double[] containersRAMReq = generateContainerRAMReq(containers);

                    double[] cpuReserved = new double[totalNodes];
                    double[] cpuTotal = new double[totalNodes];
                    double[] ramReserved = new double[totalNodes];
                    double[] ramTotal = new double[totalNodes];

                    for (int i = 0; i < totalNodes; i++) {

                        cpuReserved[i] = generateInRange(0, 15);
                        cpuTotal[i] = generateInRange(cpuReserved[i], 20);
                        ramReserved[i] = generateInRange(0, 15);
                        ramTotal[i] = generateInRange(ramReserved[i], 24);
                    }

                    Main main = new Main();

                    //--------------------------------------------------------------
                    // ACO-based implementation 
                    //--------------------------------------------------------------
                    // create the node list
                    MECServer[] servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution acoSolution = main.runACO(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    
                    
                    //--------------------------------------------------------------
                    // ACO Mobility-based implementation 
                    //--------------------------------------------------------------
                    // create the node list
                    servers = new MECServer[totalNodes];
                    
                    // Here we are creating a user for this request. We are using its location to 
                    // find the a set of MEC servers that are the closest to him.
                    // The general idea is that we should offer the services as close as possible 
                    // to the user.
                    User user = new User (new Location(100.0, 100.0));
                    
                    // We generate some random locations for the MEC servers
                    double[][] locations = GeoUtil.generateRandomLocations(servers.length, 5000, 5000, 100);

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, 
                                cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i], new Location(locations[i][0], locations[i][1]));
                    }
                    
                    Solution acoMobilitySolution = main.runACOMobility(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers, user
                    );
                    
                    
                    //--------------------------------------------------------------
                    // MaxFit implementation
                    //--------------------------------------------------------------
                    // create the node list
                    servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution maxFitSolution = main.runMaxFit(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    //--------------------------------------------------------------

                    
                    //--------------------------------------------------------------
                    // BestFit implementation
                    //--------------------------------------------------------------
                    // create the node list
                    servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution bestFitSolution = main.runBestFit(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    //--------------------------------------------------------------

                    
                    //--------------------------------------------------------------
                    // MultiOpt implementation
                    //--------------------------------------------------------------
                    // create the node list
                    servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution multiOptSolution = main.runMultiOpt(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    //--------------------------------------------------------------

                    // create the node list
                    servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution ilpSolution = main.runILP(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    //--------------------------------------------------------------

                    // create the node list
                    servers = new MECServer[totalNodes];

                    for (int i = 0; i < servers.length; i++) {
                        servers[i] = new MECServer(i, cpuReserved[i], cpuTotal[i], ramReserved[i], ramTotal[i]);
                    }

                    Solution randomSolution = main.runRandom(
                            containers, containersCPUReq, containersRAMReq, networkLatencies, servers
                    );
                    //--------------------------------------------------------------

//                    StringBuilder builder = new StringBuilder();

//                    builder.append(acoSolution.getNetworkLatency()).append(",");
//                    builder.append(acoMobilitySolution.getNetworkLatency()).append(",");
//                    builder.append(maxFitSolution.getNetworkLatency()).append(",");
//                    builder.append(bestFitSolution.getNetworkLatency()).append(",");
//                    builder.append(multiOptSolution.getNetworkLatency()).append(",");
//                    builder.append(ilpSolution.getNetworkLatency()).append(",");
//                    builder.append(randomSolution.getNetworkLatency()).append(",");

//                    builder.append(acoSolution.getExecutionTime()).append(",");
//                    builder.append(acoMobilitySolution.getExecutionTime()).append(",");
//                    builder.append(maxFitSolution.getExecutionTime()).append(",");
//                    builder.append(bestFitSolution.getExecutionTime()).append(",");
//                    builder.append(multiOptSolution.getExecutionTime()).append(",");
//                    builder.append(ilpSolution.getExecutionTime()).append(",");
//                    builder.append(randomSolution.getExecutionTime()).append(",");
                    

//                    builder.append(acoSolution.getResourceUtilisation()).append(",");
//                    builder.append(acoMobilitySolution.getResourceUtilisation()).append(",");
//                    builder.append(maxFitSolution.getResourceUtilisation()).append(",");
//                    builder.append(bestFitSolution.getResourceUtilisation()).append(",");
//                    builder.append(multiOptSolution.getResourceUtilisation()).append(",");
//                    builder.append(ilpSolution.getResourceUtilisation()).append(",");
//                    builder.append(randomSolution.getResourceUtilisation());


                    resNetworkLatency[0][iteration] =  acoSolution.getNetworkLatency();
                    resNetworkLatency[1][iteration] =  acoMobilitySolution.getNetworkLatency();
                    resNetworkLatency[2][iteration] =  maxFitSolution.getNetworkLatency();
                    resNetworkLatency[3][iteration] =  bestFitSolution.getNetworkLatency();
                    resNetworkLatency[4][iteration] =  multiOptSolution.getNetworkLatency();
                    resNetworkLatency[5][iteration] =  ilpSolution.getNetworkLatency();
                    resNetworkLatency[6][iteration] =  randomSolution.getNetworkLatency();
                    
                    resResourceUtil[0][iteration] =  acoSolution.getResourceUtilisation();
                    resResourceUtil[1][iteration] =  acoMobilitySolution.getResourceUtilisation();
                    resResourceUtil[2][iteration] =  maxFitSolution.getResourceUtilisation();
                    resResourceUtil[3][iteration] =  bestFitSolution.getResourceUtilisation();
                    resResourceUtil[4][iteration] =  multiOptSolution.getResourceUtilisation();
                    resResourceUtil[5][iteration] =  ilpSolution.getResourceUtilisation();
                    resResourceUtil[6][iteration] =  randomSolution.getResourceUtilisation();

                    resExecutionTime[0][iteration] =  acoSolution.getExecutionTime();
                    resExecutionTime[1][iteration] =  acoMobilitySolution.getExecutionTime();
                    resExecutionTime[2][iteration] =  maxFitSolution.getExecutionTime();
                    resExecutionTime[3][iteration] =  bestFitSolution.getExecutionTime();
                    resExecutionTime[4][iteration] =  multiOptSolution.getExecutionTime();
                    resExecutionTime[5][iteration] =  ilpSolution.getExecutionTime();
                    resExecutionTime[6][iteration] =  randomSolution.getExecutionTime();
                }
                
                StringBuilder builder = new StringBuilder();
                
                builder.append(mecServerPerSystem).append(",");
                
                for (int i = 0; i < resNetworkLatency.length; i++) {
                    DescriptiveStatistics stats = new DescriptiveStatistics(resNetworkLatency[i]);
                    builder.append(stats.getMean()).append(",");
                }
                
                builder.append(",");
                
                for (int i = 0; i < resResourceUtil.length; i++) {
                    DescriptiveStatistics stats = new DescriptiveStatistics(resResourceUtil[i]);
                    builder.append(stats.getStandardDeviation()).append(",");
                }
                
                builder.append(",");
                
                for (int i = 0; i < resExecutionTime.length; i++) {
                    DescriptiveStatistics stats = new DescriptiveStatistics(resExecutionTime[i]);
                    builder.append(Math.log(stats.getMean())).append(",");
                }
                
                csvWriter.append(builder.toString()).append("\n");
                
                //System.out.println("MEC SYSTEM: " + mecSystem + " MEC SERVER PER SYSTEM " + mecServerPerSystem + " -> " + builder.toString());
            }
            
            csvWriter.flush();
            csvWriter.close();
                
            System.out.println("Finished " + fileName);
        }
    }
}
