package ie.scss.tcd.enable.kubernetes.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents the metadata of a Kubernetes Node or Pod.
 * 
 */
public class Metadata {

    private String name;
    private Map<String, String> annotations;
    private Map<String, String> labels;
    
    public Metadata(){
        this.name = "";
        this.annotations = new HashMap<>();
        this.labels = new HashMap<>();
    }
    
    public Metadata(String name, Map<String, String> annotations, Map<String, String> labels) {
        this.name = name;
        this.annotations = annotations;
        this.labels = labels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }
    
    /**
     * @return the annotations
     */
    public Map<String, String> getAnnotations() {
        return annotations;
    }

    /**
     * @param annotations the annotations to set
     */
    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }
}
