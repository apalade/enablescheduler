package ie.scss.tcd.enable.kubernetes.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents an User that request an application.
 * 
 */
public class User {

    private Location location;
    private Location targetLocation;
    private Long requestTime;
    private UserType type;

    public User(Location location, Location targetLocation, Long requestTime, UserType type) {
        this.location = location;
        this.targetLocation = targetLocation;
        this.requestTime = requestTime;
        this.type = type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
    
    public Location getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(Location targetLocation) {
        this.targetLocation = targetLocation;
    }
    
    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }
    
    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}