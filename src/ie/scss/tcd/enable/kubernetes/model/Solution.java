package ie.scss.tcd.enable.kubernetes.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Solution {

    private String userType;
    private double networkLatency;
    private long executionTime;
    private double resourceUtilisation; // st deviation
    private double distance;
    private long waitingTime;
    private long kubernetesTime;

    public Solution(String userType, double networkLatency, long executionTime, double resourceUtilisation, double distance, long waitingTime, long kubernetesTime) {
        this.userType = userType;
        this.networkLatency = networkLatency;
        this.executionTime = executionTime;
        this.resourceUtilisation = resourceUtilisation;
        this.distance = distance;
        this.waitingTime = waitingTime;
        this.kubernetesTime = kubernetesTime;
    }
    
    public String getUserType() {
        return userType;
    }

    public double getResourceUtilisation() {
        return resourceUtilisation;
    }

    public double getNetworkLatency() {
        return networkLatency;
    }

    public long getExecutionTime() {
        return executionTime;
    }
    
    public double getDistance() {
        return distance;
    }
    
    public long getWaitingTime() {
        return waitingTime;
    }
    
    public long getKubernetesTime() {
        return kubernetesTime;
    }

    public String toString() {
        return networkLatency + ", " + executionTime + ", " + resourceUtilisation + ", " + distance + ", " + waitingTime + ", " + kubernetesTime;
    }
}
