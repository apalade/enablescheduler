package ie.scss.tcd.enable.kubernetes.model;

import java.math.BigDecimal;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a capacity of a given node.
 * 
 */
public class Capacity {

    private BigDecimal number;
    private Format format;
    
    public enum Format {
        DECIMAL_EXPONENT(10), DECIMAL_SI(10), BINARY_SI(2);

        private int base;

        Format(final int base) {
          this.base = base;
        }

        public int getBase() {
          return base;
        }
    }

    public Capacity(BigDecimal number, Format format) {
        this.number = number;
        this.format = format;
    }
    
    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }
}
