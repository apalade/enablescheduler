package ie.scss.tcd.enable.kubernetes.model;

import java.util.ArrayList;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents an Application that is requested.
 * 
 */
public class Application {

    private String name;
    private int priority;
    private ArrayList<Pod> pods;
    private Location location;
    private Location targetLocation;
    private Long requestTime;
    private Long prioritisationTime;
    private Long endPrioritisationTime;
    private Long delayRequirement;
    private boolean done;
    private String jobId;
    private int job;
    private int version;
    private UserType userType;
    private boolean created;
    
    public Application() {
        this.name = "";
        this.priority = 0;
        this.pods = new ArrayList<>();
        Location location = new Location(0.0,0.0);
        this.location = location;
        this.targetLocation = new Location(0.0,0.0);
        this.requestTime = (long) 0;
        this.prioritisationTime = (long) 0;
        this.endPrioritisationTime = (long) 0;
        this.delayRequirement = (long) 0;
        this.done = false;
        this.version = 0;
        this.userType = UserType.STATIC;
        this.created = false;
        this.job = 0;
    }
    
    public Application(String name, int priority, Location location, UserType userType){
        this.name = name;
        this.priority = priority;
        this.pods = new ArrayList<>();
        this.location = location;
        this.targetLocation = new Location(0.0,0.0);
        this.requestTime = (long) 0;
        this.prioritisationTime = (long) 0;
        this.endPrioritisationTime = (long) 0;
        this.delayRequirement = (long) 0;
        this.done = false;
        this.version = 0;
        this.userType = userType;
        this.created = false;
        this.job = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ArrayList<Pod> getPods() {
        return pods;
    }

    public void setPods(ArrayList<Pod> pods) {
        this.pods = pods;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }
    
    public Long getPrioritisationTime() {
        return prioritisationTime;
    }

    public void setPrioritisationTime(Long prioritisationTime) {
        this.prioritisationTime = prioritisationTime;
    }
    
    public Long getEndPrioritisationTime() {
        return endPrioritisationTime;
    }

    public void setEndPrioritisationTime(Long endPrioritisationTime) {
        this.endPrioritisationTime = endPrioritisationTime;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
    
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Long getDelayRequirement() {
        return delayRequirement;
    }

    public void setDelayRequirement(Long delayRequirement) {
        this.delayRequirement = delayRequirement;
    }
    
    public Location getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(Location targetLocation) {
        this.targetLocation = targetLocation;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }
    
    public int getJob() {
        return job;
    }

    public void setJob(int job) {
        this.job = job;
    }
}
