package ie.scss.tcd.enable.kubernetes.model;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a Kubernetes Node.
 * 
 */
public class Node {

    private Metadata metadata;
    private Status status;
    private Type type;
    
    private double cpuReserved;
    private double cpuTotal;
    private double ramReserved;
    private double ramTotal;
    private double availableRam;
    private double availableCpu;
    
    private Location location;

    public enum Type {
        WORKER, MASTER
    }
    
    public Node(){
        this.metadata = new Metadata();
        this.status = new Status();
        this.type = Type.WORKER;
    }
    
    public Node(Metadata metadata, Status status) {
        this.metadata = metadata;
        this.status = status;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void setType(Type type) {
        this.type = Type.values()[type.ordinal()];
    }
    
    public Type getType() {
        return this.type;
    } 
    
    public boolean canFit(double cpu, double ram) {
        return (cpu + getCpuReserved()) <= getCpuTotal() && (ram + getRamReserved()) <= getRamTotal();
    }
    
    public void update(double cpu, double ram) {
        cpuReserved += cpu;
        ramReserved += ram;
    }
    
    public double getCPUUtil() {
        if(cpuReserved>cpuTotal)
            return 0.9999;
        return cpuReserved / cpuTotal;
    }
    
    public double getRAMUtil() {
        if(ramReserved>ramTotal)
            return 1.0;
        return ramReserved / ramTotal;
    }
    
    public void setAvailableCpu(double availableCpu){
        this.availableCpu = availableCpu;
    }
    
    public double getAvailableCpu() {
        return availableCpu;
    }
    
    public void setAvailableRam(double availableRam){
        this.availableRam = availableRam;
    }
    
    public double getAvailableRAM() {
        return availableRam;
    }
    
    public double getCpuReserved() {
        return cpuReserved;
    }
    
    public void setCpuReserved(double cpuReserved) {
        this.cpuReserved=cpuReserved;
    }

    public double getCpuTotal() {
        return cpuTotal;
    }
    
    public void setCpuTotal(double cpuTotal) {
        this.cpuTotal = cpuTotal;
    }
    
    public double getRamReserved() {
        return ramReserved;
    }
    
    public void setRamReserved(double ramReserved) {
        this.ramReserved=ramReserved;
    }
    
    public void setRamTotal(double ramTotal) {
        this.ramTotal = ramTotal;
    }
    
    public double getRamTotal() {
        return ramTotal;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
