package ie.scss.tcd.enable.kubernetes.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents a placement decision.
 */
public class Placement {
    
    // index represents the id of the container
    // the value at a certain index represents the node where to place that container
    private int[] placement;
    
    public Placement() {
  
    }
    
    public Placement(int[] placement) {
        this.placement = placement;
    }

    public int[] getPlacement() {
        return placement;
    }
    
    public String ToJsonString(){
        String res = "";
        res = res + "{";
        for(int i=0; i<placement.length; i++){
            res = res + "node_" + (i+1) + ":" + placement[i];
            if((i+1)<placement.length)
                res = res + ",";
        }
        res = res + "}";
        return res;
    }
}
