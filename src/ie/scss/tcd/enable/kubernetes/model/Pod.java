package ie.scss.tcd.enable.kubernetes.model;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Pod {

    private Metadata metadata;
    private String node;
    
    public Pod(Metadata metadata) {
        this.metadata = metadata;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
    
    public String getNode() {
        return node;
    }
    
    public void setNode(String node) {
        this.node = node;
    }
}
