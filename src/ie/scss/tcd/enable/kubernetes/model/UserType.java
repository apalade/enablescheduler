package ie.scss.tcd.enable.kubernetes.model;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This enumaration represents the user type of the system
 * STATIC (e.g., Opera) or MOBILE (e.g., Ambulance)
 * 
 */
public enum UserType {
    STATIC, MOBILE;    
}
