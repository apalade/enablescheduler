package ie.scss.tcd.enable.kubernetes.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents the Status of a Kubernetes node.
 * 
 */
public class Status {

    private Map<String, Capacity> totalCapacity;
    private Map<String, Capacity> allocatableCapacity;
    
    public Status(){
        this.totalCapacity = new HashMap<>();
        this.allocatableCapacity = new HashMap<>();
    }
    
    public Status(Map<String, Capacity> totalCapacity, Map<String, Capacity> allocatableCapacity) {
        this.totalCapacity = totalCapacity;
        this.allocatableCapacity = allocatableCapacity;
    }

    public Map<String, Capacity> getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(Map<String, Capacity> totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public Map<String, Capacity> getAllocatableCapacity() {
        return allocatableCapacity;
    }

    public void setAllocatableCapacity(Map<String, Capacity> allocatableCapacity) {
        this.allocatableCapacity = allocatableCapacity;
    }
}
