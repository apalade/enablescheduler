package ie.scss.tcd.enable.kubernetes;

import ie.scss.tcd.enable.kubernetes.comm.interfaces.OmnetInterface;
import ie.scss.tcd.enable.kubernetes.orchestrator.adapters.KubernetesAdapter;
import java.io.IOException;
import ie.scss.tcd.enable.kubernetes.orchestrator.Orchestrator;
import ie.scss.tcd.enable.kubernetes.model.Node;
import java.util.List;
import java.util.Map;
import ie.scss.tcd.enable.kubernetes.facades.SchedulerFacade;
import ie.scss.tcd.enable.kubernetes.util.DataUtils;
import java.util.HashMap;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This is the main class of the Kubernetes package that executes the demo and the experiments
 * on top of the Kubernetes cluster.
 * 
 */
public class Main{

    private static boolean demo = false;
    public static final String KUBERNETES_CONFIG_FILE = "./config/config";
    public static final String APPLICATIONS_FILE = "./applications.xml";
    
    private static Orchestrator orchestrator;
    private static SchedulerFacade facade;
    private static OmnetInterface iface;
    
    private static List<Node> nodeList;
        
    private static Map<Integer,Map> experiments = new HashMap<>();
    private static Map<String,Double> experimentParameters;
    private static int currentExperiment = 1;
    
    public static void main(String[] args) throws IOException {
        if(demo){
            System.out.println("*** Starting Demo ***");
            //Setting parameters for ACO-based algorithm
            experimentParameters = new HashMap<>();
            experimentParameters.put("approach", 1.0);
            experimentParameters.put("nodes", 4.0);
            experimentParameters.put("functions", 3.0);
            experimentParameters.put("type", 2.0);
            experimentParameters.put("antsNumber",10.0);
            experimentParameters.put("iterations", 20000.0);
            experimentParameters.put("alfa", 0.002);
            experimentParameters.put("beta", 0.0001);
            experimentParameters.put("ro", 0.005);
            experimentParameters.put("tau0", 1.0);
            experimentParameters.put("deltaTau", 0.001);
            experimentParameters.put("wP", 0.2);
            experimentParameters.put("wM", 0.2);
            experimentParameters.put("wD", 0.2);
            experimentParameters.put("wL", 0.2);
            experimentParameters.put("wT", 0.2);
            experiments.put(1, experimentParameters);
            startDemo(1);
        }
        else{
            System.out.println("*** Starting Experiments  ***");
            int exp = 1;
            for(int approach=1; approach<=1; approach++){
                for(int nodes=4; nodes>3; nodes--){
                    for(int functions=5; functions<=5; functions++){
                        for(int type=1; type<=3; type++){
                            experimentParameters = new HashMap<>();
                            experimentParameters.put("approach", Double.parseDouble(""+approach));
                            experimentParameters.put("nodes", Double.parseDouble(""+nodes));
                            experimentParameters.put("functions", Double.parseDouble(""+functions));
                            experimentParameters.put("type", Double.parseDouble(""+type));
                            if(approach==1 || approach==2 || approach==6){
                                experimentParameters.put("antsNumber",10.0);
                                experimentParameters.put("iterations", 20000.0);
                                experimentParameters.put("alfa", 0.002);
                                experimentParameters.put("beta", 0.0001);
                                experimentParameters.put("ro", 0.005);
                                experimentParameters.put("tau0", 1.0);
                                experimentParameters.put("deltaTau", 0.001);
                                experimentParameters.put("wP", 0.2);
                                experimentParameters.put("wM", 0.2);
                                experimentParameters.put("wD", 0.2);
                                experimentParameters.put("wL", 0.2);
                                experimentParameters.put("wT", 0.2);
                            }
                            experiments.put(exp, experimentParameters);
                            exp++;
                        }
                    }
                }
            }
            startDemo(getCurrentExperiment());
        }
    }

    private static void startDemo(int experiment) {
        //Setting-up orchestrator
        if(experiments.containsKey(experiment)){
            orchestrator = new KubernetesAdapter(KUBERNETES_CONFIG_FILE);
            orchestrator.removeFunctions();
            try {
                Thread.sleep(15000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            nodeList = orchestrator.getAvailableNodes();
            System.out.println("NODE LIST: " + nodeList.size());
            for(int i=0; i<nodeList.size();i++){
                HashMap<String, String> metrics  = new HashMap<>();
                metrics.put("pods", "");
                orchestrator.updateNodeWithLabels(nodeList.get(i), metrics);
            }
            
            
            // Setting-up scheduler facade and omnet interface
            facade = new SchedulerFacade(orchestrator,demo);
            facade.setExperimentParameters(experiments.get(experiment));
            iface = new OmnetInterface(APPLICATIONS_FILE,orchestrator,facade,experiments.get(experiment));
            
            // Start listener interface
            Main m = new Main();
            iface.startListening(m);
            
            //Start scheduler thread
            facade.setIface(iface);
            facade.start();
            
            if(!demo)
                //Sending message to VM
                iface.sendExperimentTrigger(""+experiments.get(experiment).get("type"),m);
            
        }else{
            currentExperiment = currentExperiment + 1;
            startDemo(currentExperiment);
        }
        
    }

    public void finish() {
        // Stop listener interface
        iface.stopListening();
                        
        // Stop scheduler thread
        facade.interrupt();
        
        // Waiting for threads to finish
        try{
            Thread.sleep(5000);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        // Cleaning kubernetes cluster
        orchestrator.removeFunctions();
        
        // Waiting for functions deletion
        try{
            Thread.sleep(15000);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        // Writing experiment results
        System.out.println("*** Processing Experiment Results***");
        DataUtils.writeResults(currentExperiment, experiments.get(getCurrentExperiment()), facade.getResults());
        
        // Restarting next experiment
        currentExperiment++;
        if(experiments.containsKey(currentExperiment))
            startDemo(currentExperiment);
        else
            System.out.println("*** Experiments Finished ***");
    }
    
    public static int getCurrentExperiment() {
        return currentExperiment;
    }

    public static void setCurrentExperiment(int aCurrentExperiment) {
        currentExperiment = aCurrentExperiment;
    }
}
