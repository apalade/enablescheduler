package ie.scss.tcd.enable.kubernetes.scheduler.aco;


import ie.scss.tcd.enable.kubernetes.model.Capacity;
import ie.scss.tcd.enable.kubernetes.model.Placement;
import ie.scss.tcd.enable.kubernetes.model.User;
import ie.scss.tcd.enable.kubernetes.scheduler.Scheduler;
import ie.scss.tcd.enable.kubernetes.util.DataUtils;
import ie.scss.tcd.enable.kubernetes.util.GeoUtil;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.model.UserType;
import java.util.List;


/**
 * ACO implementation that uses the distance and latency to select the optimal solution
 * 
 * The niu calculation includes resources, location and latency
 * 
 * processSolution() method is where the selection is performed
 * 
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class ACOMobility_v2 implements Scheduler {
    
    // Total number of nodes
    private int nodes;
    
    // Total number of containers
    private int containers;
    
    // Total ants number
    private int antsNumber;
    
    // Ants that will explore
    private ANT[] ants;
    
    // Total number of iterations to be performed
    private int iterations;
    
    // If alfa is 0, then the closest nodes are more likely 
    // to be selected this corresponds to a classical 
    // stochastic greedy algorithm (with multiple starting 
    // points since ants are initially randomly distributed 
    // on the cities).
    // private double alfa = 1.0;
    private double alfa;
    
    // If beta is 0, then only pheromone amplification is at 
    // work this method will lead to a rapid stagnation situation
    // with the corresponding generation of tours which, in general, 
    // are strongly suboptimal 
    // private double beta = 3.0;
    private double beta;
    
    // Global evaporation rate
    // private double ro = 0.5;
    private double ro;
    
    // Initial pheromone level
    // private double tau0 = 1.0;
    private double tau0;
    
    // This is the amount of pheromone deposit used for each branch
    private double deltaTau;
    
    // Stores the pheromone levels
    private double[][] pheromone;
    
    // Stores the heuristic values
    private double[][] heuristic;
    
    // Stores the latency between nodes
    private double[][] networkLatency;
    
    // Stores the CPU utilisation
    //private double[] cpuUtil;
    
    // Stores the RAM utilisation
    //private double[] ramUtil;
    
    // Stores CPU weight
    private double wP;
    
    // Stores RAM weight
    private double wM;
    
    // Stores Latency weight
    private double wL;
    
    // Stores Distance weight
    private double wD;
    
    // Stores Waiting time weight
    private double wT;
    
    private Node[] servers;
    
    private User user;
    
    public ACOMobility_v2(int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            int antsNumber, int iterations, 
            double alfa, double beta, double ro, double tau0, double deltaTau,
            double[][] networkLatency, 
            Node[] servers, double wP, double wM, double wL, double wD, double wT) {
        
        this.servers = servers;
        this.nodes = servers.length;
        this.containers = containers;
        
        this.antsNumber = antsNumber;
        this.ants = new ANT[antsNumber];
        this.iterations = iterations;
        
        this.alfa = alfa;
        this.beta = beta;
        this.ro = ro;
        this.tau0 = tau0;
        this.deltaTau = deltaTau;
        
        this.networkLatency = networkLatency;
        
        this.wP = wP;
        this.wM = wM;
        this.wL = wL;
        this.wD = wD;
        this.wT = wT;
        
        this.pheromone = new double[nodes][containers];
        this.heuristic = new double[nodes][containers];
 
        
        initAnts();
        initPheromones();
    }
    
    public void initPheromones() {
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < containers; j++) {
                pheromone[i][j] = tau0;
            }
        }
    }
    
    public void initAnts() {
        for (int i = 0; i < antsNumber; i++) {
            this.ants[i] = new ANT(containers);
        }
    }
    
    public double[][] getPheronome() {
        return pheromone;
    }

    public void setPheronome(double[][] pheronome) {
        this.pheromone = pheronome;
    }
    
    // Basic ACO methods
    public int[] acoSchedule() {
        
        // Asign initial node to each ant
//        Random rand = new Random();
//        for (int i = 0; i < antsNumber; i++) {
//            int r = Math.abs(rand.nextInt()) % nodes;
//            ants[i].addMemory(new Memory(r, 0, cpuUtil[r], ramUtil[r]));
//        }
        
        Object object = new Object();

        int[] placements = new int[containers];
        for(int i=0; i<placements.length; i++)
            placements[i] = -1;
    
        for (int iter = 0; iter < iterations; iter++) {

            for (int j = 0; j < containers; j++) {
                for (int k = 0; k < antsNumber; k++) {
                    //computeHeuristic();
                    forwardMovement(j, k);
                }
            }

            int optimalSolutionIndex = processSolutions();
            ants[optimalSolutionIndex].setSuperior(true);
            
            for (int m = 0; m < ants[optimalSolutionIndex].getMemories().length; m++) {
                placements[m] = ants[optimalSolutionIndex].getMemories()[m].getNodeID();
            }
        
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < ants[optimalSolutionIndex].getMemories().length; i++) {
                builder.append(ants[optimalSolutionIndex].getMemories()[i].getNodeID()).append(" ");
            }
            
            //System.out.println(builder.toString());
            
            globalEvaporation();
            
            // Build the solutions
            for (int j = 0; j < containers; j++) {
                for (int k = 0; k < antsNumber; k++) {
                    backwardMovement(j, k);
                }
            }
            
            for (int k = 0; k < antsNumber; k++) {
                ants[k].reset();
            }
            
            //printPhermone();
            
        }
        
        return placements;
    }
    
    public void forwardMovement(int cIndex, int aIndex) {
        
        double sumProb = 0.0;
        double[] selectionProb = new double[nodes];
        
        for (int i = 0; i < nodes; i++) {
            
            /* Computing heuristic value including
                - CPU 
                - RAM
                - Network latency
                - Distance between nodes and app
            */
            double niu = computeHeuristic(aIndex,i);
        
            // DOUBLE CHECK THIS
            /*if (cIndex == 0) {
                niu = 1.0 / 0.01;
            }
            else {
                int curNode = ants[aIndex].getCurrentNode();
             
                if (networkLatency[curNode][i] > 0) {
                    niu = 1.0 / networkLatency[curNode][i];
                } else {
                    niu = 1.0 / 0.01;
                }
            }*/            
            selectionProb[i] =  Math.pow(pheromone[i][cIndex], alfa) * Math.pow(niu, beta);
            sumProb += selectionProb[i];
        }
        
        double prob = Math.random() * sumProb;
        int j = 0; 
        double p = selectionProb[j];
        while(p < prob) {
            j++;
            p += selectionProb[j];
        }
        
        if (cIndex == 0) {
            
            ants[aIndex].addMemory(cIndex, 
                    new ANTMemory(j, networkLatency[j][j], servers[j].getCPUUtil(), 
                            servers[j].getRAMUtil(), servers[j].getLocation()));
        }
        else {
            
            int cNode = ants[aIndex].getCurrentNode();
            
            ants[aIndex].addMemory(cIndex, 
                    new ANTMemory(j, networkLatency[cNode][j], servers[j].getCPUUtil(), 
                            servers[j].getRAMUtil(), servers[j].getLocation()));
        }
        ants[aIndex].setCurrentNode(j);
        
//        if (aIndex == 0) {
//            System.out.println(j);
//        }
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public void backwardMovement(int cIndex, int aIndex) {
        depositPheronome(cIndex, aIndex);
    }
    
    public int processSolutions() {
    
        double[] latencyValues = new double[antsNumber];
        double[] requestDistanceValues = new double[antsNumber];
        double[] targetDistanceValues = new double[antsNumber];
        
        for (int i = 0; i < antsNumber; i++) {
            
            double totalLatency = 0;
            double totalRequestDistance = 0;
            double totalTargetDistance = 0;
            
            for (int j = 0; j < ants[i].getMemories().length; j++) {
                totalLatency += ants[i].getMemories()[j].getLatency();
                totalRequestDistance += GeoUtil.getDistance(
                    user.getLocation().getX(), 
                    user.getLocation().getY(), 
                    ants[i].getMemories()[j].getLocation().getX(), 
                    ants[i].getMemories()[j].getLocation().getY());
                if(user.getType() == UserType.MOBILE)
                    totalTargetDistance += GeoUtil.getDistance(
                        user.getTargetLocation().getX(), 
                        user.getTargetLocation().getY(), 
                        ants[i].getMemories()[j].getLocation().getX(), 
                        ants[i].getMemories()[j].getLocation().getY());
            }
            
            latencyValues[i] = totalLatency;
            requestDistanceValues[i] = totalRequestDistance;
            targetDistanceValues[i] = totalTargetDistance;
        }
        
        double[] normLatency = DataUtils.normalize(latencyValues);
        double[] normRequestDistance = DataUtils.normalize(requestDistanceValues);
        double[] normTargetDistance = DataUtils.normalize(targetDistanceValues);
        
        double minUtil = -1;
        int minUtilIndex = -1;
        
        // We are minimising the utility since we are minimising both latency and distances
        for (int i = 0; i < normLatency.length; i++) {
            double tempUtil = 0;
            if(user.getType() == UserType.STATIC)
               tempUtil = normRequestDistance[i] * 0.5 + normLatency[i] * 0.5;
            if(user.getType() == UserType.MOBILE)
                tempUtil = normTargetDistance[i] * 0.333 + normRequestDistance[i] * 0.333 + normLatency[i] * 0.333;
            
            
            if (minUtil == -1) {
                minUtil = tempUtil;
                minUtilIndex = i;
            }
            else {
                if (tempUtil < minUtil) {
                    minUtil = tempUtil;
                    minUtilIndex = i;
                }
            }
        }
         
        return minUtilIndex;
    }
    
    public void depositPheronome(int cIndex, int aIndex) {
        
        if (ants[aIndex].isSuperior()) {
            int nodeID = ants[aIndex].getMemories()[cIndex].getNodeID();
            pheromone[nodeID][cIndex] = pheromone[nodeID][cIndex] + deltaTau;
        }
    }
    
    public double computeHeuristic(int aIndex, int nIndex) {
        double niu=0.0;
        
        /*
            CPU and RAM values of future node
        */
        Capacity allocatableCPU = servers[nIndex].getStatus().getAllocatableCapacity().get("cpu");
        Capacity totalCPU = servers[nIndex].getStatus().getTotalCapacity().get("cpu");
        Double cpu = allocatableCPU.getNumber().doubleValue()/totalCPU.getNumber().doubleValue();
        
        Capacity allocatableRAM = servers[nIndex].getStatus().getAllocatableCapacity().get("memory");
        Capacity totalRAM = servers[nIndex].getStatus().getTotalCapacity().get("memory");
        Double ram = allocatableRAM.getNumber().doubleValue()/totalRAM.getNumber().doubleValue();
        
        /*
            Network latency between current node and future node
        */
        Double latency = 0.0;
        if(ants[aIndex].getCurrentNode()!=-1){
            int curNode = ants[aIndex].getCurrentNode();
            if(networkLatency[curNode][nIndex]>0)
                latency = 1.0 / networkLatency[curNode][nIndex];
            else
                latency = 1.0 / 0.0001;
        }
        
        /*
            Distance between app request and future node
        */
        Double distanceRequest = GeoUtil.getDistance(user.getLocation().getX(), user.getLocation().getY(),
                servers[nIndex].getLocation().getX(), servers[nIndex].getLocation().getY());
        if(distanceRequest>0)
            distanceRequest = 1.0/distanceRequest;
        
        /*
            Distance between app target and future node
        */
        Double distanceTarget = 0.0;
        if(user.getType() == UserType.MOBILE)
            distanceTarget = GeoUtil.getDistance(user.getTargetLocation().getX(), user.getTargetLocation().getY(),
                servers[nIndex].getLocation().getX(), servers[nIndex].getLocation().getY());
        if(distanceTarget>0)
            distanceTarget = 1.0/distanceTarget;
        
        if(user.getType() == UserType.MOBILE)
            niu = (wP * cpu) + (wM * ram) + (wL * latency) + (wD * distanceRequest) + (wT * distanceTarget);
        else{
            double dT = wT/4;
            niu = ((wP+dT) * cpu) + ((wM+dT) * ram) + ((wL+dT) * latency) + ((wD+dT) * distanceRequest);
        }
        return niu;
    }
    
    // Decrease the pheromone level in the pheron
    public void globalEvaporation() {
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < containers; j++) {
                pheromone[i][j] = (1 - ro) * pheromone[i][j];
            }
        }
    }
    
    public void getAntLocation() {
        for (int i = 0; i < ants.length; i++) {
            System.out.println("Ant " + i + " ");
        }
    }
    
    private void printPhermone() {
        
        for (int i = 0; i < pheromone.length; i++) {
            for (int j = 0; j < pheromone[i].length; j++) {
                System.out.print(pheromone[i][j] + " ");
            }
            System.out.println();
        }
        
        System.out.println();
        System.out.println();
    }
    
    @Override
    public Placement schedule() {

        int[] solution = acoSchedule();

        Placement placement = new Placement(solution);
        return placement;
    }

} 
