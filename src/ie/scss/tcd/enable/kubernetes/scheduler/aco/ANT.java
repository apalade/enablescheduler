package ie.scss.tcd.enable.kubernetes.scheduler.aco;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class represents an ANT that goes through the graph of nodes in the search process.
 * 
 */
public class ANT {
    
    // If an ant is superior, is allowed to deposit pheromone
    private boolean superior;
    
    // The memories are stored in stack
    private ANTMemory[] memories; 
    
    // Current node
    private int currentNode;
    
    public ANT(int size) {
        this.memories = new ANTMemory[size];
        this.superior = false;
        this.currentNode = -1;
    }

    public ANTMemory[] getMemories() {
        return memories;
    }

    public void setMemories(ANTMemory[] memories) {
        this.memories = memories;
    }
    
    public void addMemory(int index, ANTMemory memory) {
        memories[index] = memory;
    }

    public boolean isSuperior() {
        return superior;
    }

    public void setSuperior(boolean superior) {
        this.superior = superior;
    }

    public int getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(int currentNode) {
        this.currentNode = currentNode;
    }
    
    public void reset() {
        this.currentNode = -1;
        this.superior = false;
    }
}
