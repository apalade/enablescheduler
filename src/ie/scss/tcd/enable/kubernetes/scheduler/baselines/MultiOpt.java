package ie.scss.tcd.enable.kubernetes.scheduler.baselines;

import ie.scss.tcd.enable.kubernetes.model.Placement;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.scheduler.Scheduler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the MultiOpt baseline
 * 
 */
public class MultiOpt extends Heuristic implements Scheduler {

    public MultiOpt(int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency, 
            Node[] servers) {
     
        super(containers, containersCPUReq, containersRAMReq, 
                networkLatency, servers);
    }

    class Result implements Comparable<Result> {
        
        private int id;
        private double score;
        
        public Result(int id, double score) {
            this.id = id;
            this.score = score;
        }
        
        public int getID() {
            return id;
        }
        
        public double getScore() {
            return score;
        }

        @Override
        public int compareTo(Result arg0) {
            return this.score < arg0.getScore() ? -1 : this.score == arg0.getScore() ? 0 : 1;
        }
    }
    
    @Override
    public Placement schedule() {
        
        int containers = getContainers();
        double[] containersCPUReq = getContainersCPUReq();
        double[] containersRAMReq = getContainersRAMReq();
        Node[] servers = getServers();
        
        int[] placement = new int[containers];
        for(int i=0; i<placement.length; i++)
            placement[i] = -1;

        for (int i = 0; i < placement.length; i++) {
        
            List<Result> results = new ArrayList<>();

            for (int j = 0; j < servers.length; j++) {
                double num=servers[j].getCPUUtil() * 100;
                double den=(1-servers[j].getCPUUtil()) * 100;
                double rateCPU = num/den;
                num=servers[j].getRAMUtil() * 100;
                den=(1-servers[j].getRAMUtil()) * 100;
                double rateRAM = num/den;
                double score = 0.5 * rateCPU + 0.5 * rateRAM;
                results.add(new Result(j, score));
            }
            
            Collections.sort(results);
            
            placement[i] = results.get(0).getID();
            servers[results.get(0).getID()].update(containersCPUReq[i], containersRAMReq[i]);
        }
        
        Placement solution = new Placement(placement);
        return solution;
    }
    
}
