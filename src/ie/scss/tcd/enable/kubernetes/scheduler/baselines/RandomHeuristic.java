package ie.scss.tcd.enable.kubernetes.scheduler.baselines;

import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Placement;
import ie.scss.tcd.enable.kubernetes.scheduler.Scheduler;
import java.util.Random;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the Random baseline
 * 
 */
public class RandomHeuristic extends Heuristic implements Scheduler {
    
    
    public RandomHeuristic(int containers,
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency,
            Node[] servers) {

        super(containers, containersCPUReq, containersRAMReq,
                networkLatency, servers);
    }
    
    @Override
    public Placement schedule() {
        
        int containers = getContainers();
        Node[] servers = getServers();
        
        int[] placements = new int[containers];
        for(int i=0; i<placements.length; i++)
            placements[i] = -1;
        
        Random random = new Random();
        
        for (int i = 0; i < containers; i++) {
            placements[i] = random.nextInt(servers.length);
        }
        
        Placement solution = new Placement(placements);
        return solution;
    }

}
