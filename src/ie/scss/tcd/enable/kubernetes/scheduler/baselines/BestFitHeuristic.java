package ie.scss.tcd.enable.kubernetes.scheduler.baselines;

import ie.scss.tcd.enable.kubernetes.model.Placement;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.scheduler.Scheduler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the BestFit baseline
 * 
 */
public class BestFitHeuristic extends Heuristic implements Scheduler {
    
    public BestFitHeuristic(int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency, 
            Node[] servers) {
     
        super(containers, containersCPUReq, containersRAMReq, 
                networkLatency, servers);
    }
    
    @Override
    public Placement schedule() {
        
        int containers = getContainers();
        double[] containersCPUReq = getContainersCPUReq();
        double[] containersRAMReq = getContainersRAMReq();
        
        Node[] servers = getServers();
        
        List<Node> nodes = new ArrayList<>();

        for (int i = 0; i < servers.length; i++) {
            nodes.add(servers[i]);
        }
        
        // create the placement list
        int[] placements = new int[containers];
        
        for (int i = 0; i < containers; i++) {
            
            for (int j = 0; j < nodes.size(); j++) {
                if (nodes.get(j).canFit(containersCPUReq[i], containersRAMReq[i])) {
                    nodes.get(j).update(containersCPUReq[i], containersRAMReq[i]);
                    placements[i] = j;
                    break;
                }
            }
        }
        
        Placement solution = new Placement(placements);
        return solution;
    }
}
