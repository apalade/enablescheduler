package ie.scss.tcd.enable.kubernetes.scheduler.baselines;

import ie.scss.tcd.enable.kubernetes.model.Node;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public class Heuristic {
    
    private Node[] servers;
    private int containers;
    
    private double[] containersCPUReq;
    private double[] containersRAMReq;
    private double[][] networkLatency;
    
    public Heuristic (int containers, 
            double[] containersCPUReq, double[] containersRAMReq,
            double[][] networkLatency, 
            Node[] servers) {
        
        this.containers = containers;
        this.containersCPUReq = containersCPUReq;
        this.containersRAMReq = containersRAMReq;
        this.networkLatency = networkLatency;
        this.servers = servers;
    }
    
    public int getContainers() {
        return containers;
    }

    public double[] getContainersCPUReq() {
        return containersCPUReq;
    }

    public double[] getContainersRAMReq() {
        return containersRAMReq;
    }

    public double[][] getNetworkLatency() {
        return networkLatency;
    }

    public Node[] getServers() {
        return servers;
    }
}
