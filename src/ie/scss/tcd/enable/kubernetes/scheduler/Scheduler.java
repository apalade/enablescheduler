package ie.scss.tcd.enable.kubernetes.scheduler;

import ie.scss.tcd.enable.kubernetes.model.Placement;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public interface Scheduler {
    
    public Placement schedule();
}
