package ie.scss.tcd.enable.kubernetes.util;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements utilities that are used across the system
 * 
 */
public class FormatUtils {
    
    private static final Gson GSON = new Gson();

    public static String jsonToString(Object object) {
        return GSON.toJson(object);
    }
    
    public static <T> T jsonToObject(String json, Type type) {
        
        T object = null;
        
        try {
            object = GSON.fromJson(json, type);
        }
        catch (JsonParseException e) {
            //throw new YourException();
            System.out.println(e);
        }
        return object;
    }
}
