package ie.scss.tcd.enable.kubernetes.util;

import ie.scss.tcd.enable.kubernetes.model.Application;
import ie.scss.tcd.enable.kubernetes.model.Location;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Metadata;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.model.Solution;
import ie.scss.tcd.enable.kubernetes.model.UserType;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements utilities that are used across the system
 * 
 */
public class DataUtils {

    /**
     * Normalizes the elements in the array using the highest value from the array
     * 
     * @param input The array to be normalized
     * @return The normalized array
     */
    public static double[] normalize(double[] input) {
        
        double[] normalised = new double[input.length];
        
        double max = Double.MIN_VALUE;
        
        for (int i = 0; i < input.length; i++) {
            if (max < input[i]) {
                max = input[i];
            }
        }
        
        for (int i = 0; i < input.length; i++) {
            normalised[i] = input[i] / max;
        }
        
        return normalised;
    }
    
    /**
     * Retrieves the index of the highest element from this array
     * 
     * @param input The input element to be searched
     * @return The index of the highest element
     */
    public static int getMaxIndex(double[] input) {
        
        int index = 0;
        double max = input[0];
        
        for (int i = 0; i < input.length; i++) {
            if (max < input[i]) {
                max = input[i];
                index = i;
            }
        }
        
        return index;
    }

    /**
     * Read applications from a XML file that has their descriptions.
     * @param fileName The file path
     * @return The list of applications
     **/
    public static ArrayList<Application> readApplications(String fileName) {
        ArrayList<Application> applications = new ArrayList<>();
        try{
            File file = new File(fileName);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();
            NodeList d = doc.getElementsByTagName("applications");
            for(int i=0; i<d.getLength();i++){
                org.w3c.dom.Node mainNode = d.item(i);
                Element element = (Element)mainNode;
                NodeList apps = element.getElementsByTagName("application");
                for(int j=0;j<apps.getLength();j++){
                    element = (Element)apps.item(j);
                    String appName = element.getAttribute("name");
                    int appPriority = Integer.parseInt(element.getAttribute("priority"));
                    Double lat = Double.parseDouble(element.getAttribute("lat"));
                    Double longitude = Double.parseDouble(element.getAttribute("long"));
                    Location appLocation = new Location(lat, longitude);
                    Application application = new Application(appName, appPriority, appLocation, UserType.STATIC);
                    NodeList services = element.getChildNodes();
                    for(int x=0; x<services.getLength();x++){
                        org.w3c.dom.Node ser = services.item(x);
                        if(ser.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
                            element = (Element)ser;
                        
                            Map<String,String> labels = new HashMap<>();
                            String serName = element.getAttribute("name");
                            labels.put("zone", element.getAttribute("zone"));
                            labels.put("version", element.getAttribute("version"));
                            
                            Map<String,String> annotations = new HashMap<>();
                            annotations.put("application",appName);
                            annotations.put("next", element.getAttribute("next"));
                            annotations.put("scheduler", element.getAttribute("scheduler"));   
                            annotations.put("priority","" + appPriority);   
                            
                            NodeList conts = element.getChildNodes(); 
                            for(int z=0; z<conts.getLength(); z++){
                                org.w3c.dom.Node cont = conts.item(z);
                                if(cont.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
                                    element = (Element) cont;
                                    annotations.put("container-name", element.getAttribute("name"));
                                    annotations.put("container-image", element.getAttribute("image"));
                                    annotations.put("container-port", element.getAttribute("port"));
                                }
                            }
                            Metadata metadata = new Metadata(serName, annotations, labels);
                            Pod pod = new Pod(metadata);
                            application.getPods().add(pod);
                        }
                    }
                    applications.add(application);
                }
            }
        }catch(Exception ex){
            System.out.println("Problems reading applications file!!!");
            ex.printStackTrace();
        }
        return applications;
    }
    
    /**
     * Aggregates the network latency values of a given placement
     * @param placements The placement decision
     * @param networkLatency The network latency between nodes
     * @return The aggregated network latency
     **/
    public static double aggregateNetworkLatency(int[] placements, double[][] networkLatency) {

        double sum = 0;

        for (int i = 1; i < placements.length; i++) {
            sum += networkLatency[placements[i - 1]][placements[i]];
        }

        return sum;
    }
    
    /**
     * Aggregates the resource utilisation values of a given placement
     * @param placements The placement decision
     * @param containerCPUReq The cpu requirements of the containers
     * @param containerRAMReq The ram requirements of the containers
     * @param servers List of available servers
     * @return The aggregated resources utilisation
     **/
    public static double getResourceUtilisation(int[] placements,
            double[] containerCPUReq, double[] containerRAMReq, Node[] servers) {

        double[] cpuUtil = new double[servers.length];
        double[] ramUtil = new double[servers.length];

        for (int i = 0; i < servers.length; i++) {
            cpuUtil[i] = servers[i].getCPUUtil();
            ramUtil[i] = servers[i].getRAMUtil();
        }

        for (int i = 0; i < placements.length; i++) {

            double cpuReq = servers[placements[i]].getCpuReserved();
            double cpuTotal = servers[placements[i]].getCpuTotal();
            cpuUtil[placements[i]] = (cpuReq + containerCPUReq[i]) / cpuTotal;

            double ramReq = servers[placements[i]].getRamReserved();
            double ramTotal = servers[placements[i]].getRamTotal();
            ramUtil[placements[i]] = (ramReq + containerRAMReq[i]) / ramTotal;
        }

        DescriptiveStatistics cpuStats = new DescriptiveStatistics(cpuUtil);
        DescriptiveStatistics ramStats = new DescriptiveStatistics(cpuUtil);

        return Math.sqrt(
                0.5 * Math.pow(cpuStats.getStandardDeviation(), 2)
                + 0.5 * Math.pow(ramStats.getStandardDeviation(), 2));
    }
    
    /**
     * Calculates the distance metric of a given placement
     * @param placements The placement decision
     * @param application The placed application
     * @param servers List of available servers
     * @return The aggregated resources utilisation
     **/
    public static double getDistanceMetric(int[] placements, Application application, Node[] servers) {
        Location[] serverLocations = new Location[servers.length];
        
        for (int i = 0; i < servers.length; i++) {
            serverLocations[i] = servers[i].getLocation();
        }
        
        Double distanceToOrigin = 0.0;
        Double distanceToTarget = 0.0;
        for (int i = 0; i < placements.length; i++) {
            Location serverLocation = serverLocations[placements[i]];
            distanceToOrigin = distanceToOrigin + GeoUtil.getDistance(application.getLocation().getX(), 
                                                                        application.getLocation().getY(), 
                                                                        serverLocation.getX(), 
                                                                        serverLocation.getY());
            if(application.getUserType()==UserType.MOBILE)
                distanceToTarget = distanceToTarget + GeoUtil.getDistance(application.getTargetLocation().getX(), 
                                                                        application.getTargetLocation().getY(), 
                                                                        serverLocation.getX(), 
                                                                        serverLocation.getY());
        }
        
        if(application.getUserType()==UserType.STATIC)
            return (distanceToOrigin / placements.length);
        if(application.getUserType()==UserType.MOBILE)
            return (((distanceToOrigin / placements.length) + (distanceToTarget / placements.length)) / 2);
        return 0;
    }

    /**
     * Writes the results file
     * @param currentExperiment Experiment identifier
     * @param experimentParameters Experiment parameters
     * @param results Experiment results
     **/
    public static void writeResults(int currentExperiment, Map<String, Double> experimentParameters, List<Solution> results) {
        try{
            String fileName = "output/results.csv";
            FileWriter csvWriter = new FileWriter(fileName,true);
            int r = 0;
            List<Solution> filteredResults = new ArrayList<Solution>();
            for(int i=0;i<results.size() && r<100;i++){
                if(results.get(i).getUserType()==UserType.MOBILE.toString()){
                    filteredResults.add(results.get(i));
                    r++;
                }                      
            }
            
            for(int i=0;i<results.size() && r<100; i++){
                if(results.get(i).getUserType()==UserType.STATIC.toString()){
                    filteredResults.add(results.get(i));
                    r++;
                }                      
            }
            
            if(currentExperiment==1){
                StringBuilder builder = new StringBuilder();
                builder.append("experiment").append(",");
                builder.append("type").append(",");
                builder.append("approach").append(",");
                builder.append("nodes").append(",");
                builder.append("functions").append(",");
                builder.append("user").append(",");
                builder.append("network-latency").append(",");
                builder.append("waiting-time").append(",");
                builder.append("execution-time").append(",");
                builder.append("kubernetes-time").append(",");
                builder.append("utilisation").append(",");
                builder.append("distance").append(",");
                csvWriter.append(builder.toString()).append("\n");
            }
            
            for(int i=0; i<filteredResults.size(); i++){
                StringBuilder builder = new StringBuilder();
                builder.append(currentExperiment).append(",");
                builder.append(experimentParameters.get("type").intValue()).append(",");
                int ap=experimentParameters.get("approach").intValue();
                String approach="";
                switch(ap){
                    case 1:
                        approach = "MA-ACO1";
                        break;
                    case 2:
                        approach = "ACO";
                        break;
                    case 3:
                        approach = "MaxFit";
                        break;
                    case 4:
                        approach = "MultiOpt";
                        break;
                    case 5:
                        approach = "Random";
                        break;
                    case 6:
                        approach = "MA-ACO2";
                        break;
                    case 7:
                        approach = "BestFit";
                        break;
                }
                builder.append(approach).append(",");
                builder.append(experimentParameters.get("nodes").intValue()).append(",");
                builder.append(experimentParameters.get("functions").intValue()).append(",");
                builder.append(filteredResults.get(i).getUserType()).append(",");
                builder.append(filteredResults.get(i).getNetworkLatency()).append(",");
                builder.append(filteredResults.get(i).getWaitingTime()).append(",");
                builder.append(filteredResults.get(i).getExecutionTime()).append(",");
                builder.append(filteredResults.get(i).getKubernetesTime()).append(",");
                builder.append(filteredResults.get(i).getResourceUtilisation()).append(",");
                builder.append(filteredResults.get(i).getDistance()).append(",");
                csvWriter.append(builder.toString()).append("\n");
            }
            
            csvWriter.flush();
            csvWriter.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }                    
    }
}
