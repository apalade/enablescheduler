package ie.scss.tcd.enable.kubernetes.facades;

import ie.scss.tcd.enable.kubernetes.comm.interfaces.OmnetInterface;
import ie.scss.tcd.enable.kubernetes.model.Application;
import ie.scss.tcd.enable.kubernetes.model.Location;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Placement;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.model.User;
import ie.scss.tcd.enable.kubernetes.orchestrator.Orchestrator;
import ie.scss.tcd.enable.kubernetes.scheduler.aco.ACOMobility_v2;
import ie.scss.tcd.enable.kubernetes.util.DataUtils;
import ie.scss.tcd.enable.kubernetes.model.Solution;
import ie.scss.tcd.enable.kubernetes.orchestrator.adapters.KubernetesAdapter;
import ie.scss.tcd.enable.kubernetes.scheduler.baselines.BestFitHeuristic;
import ie.scss.tcd.enable.kubernetes.scheduler.baselines.MaxFitHeuristic;
import ie.scss.tcd.enable.kubernetes.scheduler.baselines.MultiOpt;
import ie.scss.tcd.enable.kubernetes.scheduler.baselines.RandomHeuristic;
import ie.scss.tcd.enable.kubernetes.scheduler.aco.ACO;
import ie.scss.tcd.enable.kubernetes.scheduler.aco.ACOMobility_v1;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;


/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * 
 * This facade set-up the applications and Kubernetes parameters before running the placement algorithms
 * It places applications pods according to the algorithms output.
 * 
 */
public class SchedulerFacade extends Facade{ 

    private int approach;
    private int nodes;
    private int antsNumber;
    private int iterations;
    private int functions;
    private int type;
    private Double alfa;
    private Double beta;
    private Double ro;
    private Double tau0;
    private Double deltaTau;
    private Double wP;
    private Double wM;
    private Double wL;
    private Double wD;
    private Double wT;    
    private int app;
    private boolean demo = false;
    
    private Map<String,Application> applicationsQueue = new HashMap<>();
    private Map<String,Application> scheduledApps = new HashMap<>();
    private Map<String,Pod> functionsToRemove = new HashMap<>(); 
    private List<Solution> results = new ArrayList();
    String applicationMetrics="";
            
    private Orchestrator controller;
    private OmnetInterface iface;
    
    public SchedulerFacade(Orchestrator controller, boolean demo){
        this.controller = controller;
        this.applicationsQueue = new HashMap<>();
        this.scheduledApps = new HashMap<>();
        this.functionsToRemove = new HashMap<>(); 
        this.app = -1;
        this.demo = demo;
        this.results = new ArrayList<>(); 
    }
    
    /*
        Thread that constantly checks applications to schedule.
    */
    @Override
    public void run(){
        while(!interrupted())
            this.scheduleApplications();
    }    
    
    private void scheduleApplications() {
        Map<Integer,Application> prioritisedApps = prioritiseApps(this.applicationsQueue);
        applicationsQueue = new HashMap<>();
        for(int i=1; i<=prioritisedApps.size(); i++){
            Placement solution = new Placement();
            Application application = prioritisedApps.get(i);
            long startingPlacement = System.nanoTime();
            long waitingTime  = startingPlacement - application.getRequestTime();
            //Pods parameters for a given app
            List<Pod> appPods = application.getPods();
            double[] containersCPUReq = generateContainerCPUReq(appPods.size());
            for(int j=0; j<containersCPUReq.length; j++)
                application.getPods().get(j).getMetadata().getAnnotations().put("cpu",""+containersCPUReq[j]);
            double[] containersRAMReq = generateContainerRAMReq(appPods.size());
            for(int j=0; j<containersRAMReq.length; j++)
                application.getPods().get(j).getMetadata().getAnnotations().put("ram",""+containersRAMReq[j]);
            double[][] networkLatencies = controller.getNetworkLatencies();
            if(!application.isCreated()){
                controller.createFunction(application);
                application.setCreated(true);
            }
            
            //Servers parameters
            List<Node> nodeList = controller.getAvailableNodes();
            if(nodeList.size()>0){
                Node[] servers = nodeList.toArray(new Node[nodeList.size()]);
                //Generated latencies if they do not provide from Omnet++
                if(networkLatencies==null)
                    networkLatencies = generateNetworkLatencies(servers.length);
                //Generated locantions if they do not provide from Omnet++
                for(int j=0; j<servers.length; j++){
                    if(servers[j].getLocation()==null){
                        double longitudeApp = application.getLocation().getX();
                        double latitudeApp = application.getLocation().getY();
                        double dx = generateInRange(50,100);
                        double dy = generateInRange(50,100);
                        double minx = longitudeApp - dx;
                        double maxx = longitudeApp + dx;
                        double miny = latitudeApp - dy;
                        double maxy = latitudeApp + dy;
                        double longitude = generateInRange(minx,maxx);
                        double latitude = generateInRange(miny,maxy);
                        Location location = new Location(longitude,latitude);
                        servers[j].setLocation(location);
                    }
                }
            
                long startExecution = System.nanoTime();
                switch(this.approach){
                    case 1:
                        ACOMobility_v1 maaco1 = new ACOMobility_v1(
                            appPods.size(), containersCPUReq, containersRAMReq,
                            antsNumber, iterations,
                            alfa, beta, ro, tau0, deltaTau,
                            networkLatencies, servers, wP, wM, wL, wD, wT
                        );
                        
                        //Configuring user according to app properties (location and request time)
                        User user = new User(application.getLocation(), application.getTargetLocation(), application.getRequestTime(), application.getUserType());
                        maaco1.setUser(user);
                        //MA=ACO v1 is executed
                        solution = maaco1.schedule();
                        break;
                    case 2:
                        ACO aco = new ACO(
                            appPods.size(), containersCPUReq, containersRAMReq,
                            antsNumber, iterations,
                            alfa, beta, ro, tau0, deltaTau,
                            networkLatencies, servers
                        );
                        
                        //ACO is executed
                        solution = aco.schedule();
                        break;
                    case 3:
                        MaxFitHeuristic maxFitHeuristic = new MaxFitHeuristic(
                            appPods.size(), containersCPUReq, containersRAMReq,networkLatencies, servers);
                        solution = maxFitHeuristic.schedule();
                        break;
                    case 4:
                        MultiOpt multiOpt = new MultiOpt(
                                appPods.size(), containersCPUReq, containersRAMReq,networkLatencies, servers);
                        solution = multiOpt.schedule();
                        break;
                    case 5:
                        RandomHeuristic random = new RandomHeuristic(appPods.size(), containersCPUReq, containersRAMReq,networkLatencies, servers);
                        solution = random.schedule();
                        break;
                    case 6:
                        ACOMobility_v2 maaco2 = new ACOMobility_v2(
                            appPods.size(), containersCPUReq, containersRAMReq,
                            antsNumber, iterations,
                            alfa, beta, ro, tau0, deltaTau,
                            networkLatencies, servers, wP, wM, wL, wD, wT
                        );
                        
                        //Configuring user according to app properties (location and request time)
                        user = new User(application.getLocation(), application.getTargetLocation(), application.getRequestTime(), application.getUserType());
                        maaco2.setUser(user);
                        //MA-ACO v2 is executed
                        solution = maaco2.schedule();
                        break;
                    case 7:
                        BestFitHeuristic bestFit = new BestFitHeuristic(appPods.size(), containersCPUReq, containersRAMReq,networkLatencies, servers);
                        solution = bestFit.schedule();
                        break;
                }
                long finishExecutiom = System.nanoTime();
                long executionTime = finishExecutiom - startExecution;    
            
                //Placing each function according to algorithm output and if therer are available nodes
                boolean readyToDeploy = true;
                for(int x=0; x<solution.getPlacement().length; x++){
                    if(solution.getPlacement()[x]==-1){
                        readyToDeploy=false;
                        break;
                    }
                }
                if(readyToDeploy && nodeList.size()>0){
                    long startingKubernetes = System.nanoTime();
                    for(int x=0; x<solution.getPlacement().length; x++){
                        String podName = appPods.get(x).getMetadata().getName();
                        String nodeName= servers[solution.getPlacement()[x]].getMetadata().getName();
                        double pr = Double.parseDouble(appPods.get(x).getMetadata().getAnnotations().get("ram"));
                        double pc = Double.parseDouble(appPods.get(x).getMetadata().getAnnotations().get("cpu"));
                        controller.schedule(nodeName, podName, pr,pc);
                        appPods.get(x).setNode(nodeName);
                        print(approach, nodeList, appPods, podName, nodeName);
                        if(demo){
                            double networkLatency = DataUtils.aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
                            double resourceUtilisation = DataUtils.getResourceUtilisation(solution.getPlacement(), containersCPUReq, containersRAMReq, servers);
                            double distance = DataUtils.getDistanceMetric(solution.getPlacement(), application,servers);
                            if(applicationMetrics.length()==0)
                                applicationMetrics = application.getJobId() + "," + networkLatency + "," + distance + "," + resourceUtilisation;
                            else
                                applicationMetrics = applicationMetrics + ";" + application.getJobId() + "," + networkLatency + "," + distance + "," + resourceUtilisation;
                            HashMap<String, String> metrics  = new HashMap<>();
                            metrics.put("pods", applicationMetrics);
                            controller.updateNodeWithLabels(servers[solution.getPlacement()[x]], metrics);
                        }
                    }
                    long endingKubernetes = System.nanoTime();
                    long kubernetesTime = endingKubernetes - startingKubernetes;
                    this.scheduledApps.put(application.getJobId(), application);
                    iface.sendPlacementToOmnet(solution.ToJsonString());
                    double networkLatency = DataUtils.aggregateNetworkLatency(solution.getPlacement(), networkLatencies);
                    double resourceUtilisation = DataUtils.getResourceUtilisation(solution.getPlacement(), containersCPUReq, containersRAMReq, servers);
                    double distance = DataUtils.getDistanceMetric(solution.getPlacement(), application,servers);
                    Solution result = new Solution(application.getUserType().toString(), networkLatency, executionTime, resourceUtilisation,distance, waitingTime,kubernetesTime);
                    results.add(result);
                }else{
                    this.addApplicationToQueue(application);
                }
            }else{
                this.addApplicationToQueue(application);
            }
        }
        prioritisedApps = new HashMap<>();
        try {
            this.wait(1);
        } catch (Exception ex) {
            
        }
    }
    
    public void setExperimentParameters(Map<String, Double> experimentParameters) {
        this.approach = experimentParameters.get("approach").intValue();
        this.nodes = experimentParameters.get("nodes").intValue();
        this.functions = experimentParameters.get("functions").intValue();
        this.type = experimentParameters.get("type").intValue();
        if(approach==1 || approach==2 || approach==6){
            this.antsNumber = experimentParameters.get("antsNumber").intValue();
            this.iterations = experimentParameters.get("iterations").intValue();
            this.alfa = experimentParameters.get("alfa");
            this.beta = experimentParameters.get("beta");
            this.ro = experimentParameters.get("ro");
            this.tau0 = experimentParameters.get("tau0");
            this.deltaTau = experimentParameters.get("deltaTau");
            this.wP = experimentParameters.get("wP");
            this.wM = experimentParameters.get("wM");
            this.wL = experimentParameters.get("wL");
            this.wD = experimentParameters.get("wD");
            this.wT = experimentParameters.get("wT");
        }
    }
    
    public Map<String, Double> getExperimentParameters() {
        Map<String, Double> experimentParameters = new HashMap<>();
        experimentParameters.put("nodes", (double)this.nodes);
        experimentParameters.put("antsNumber", (double)this.antsNumber);
        experimentParameters.put("iterations", (double)this.iterations);
        experimentParameters.put("alfa", this.alfa);
        experimentParameters.put("beta", this.beta);
        experimentParameters.put("ro", this.ro);
        experimentParameters.put("tau0", this.tau0);
        experimentParameters.put("deltaTau", this.deltaTau);
        experimentParameters.put("wP", this.wP);
        experimentParameters.put("wM", this.wM);
        experimentParameters.put("wL", this.wL);
        experimentParameters.put("wD", this.wD);
        experimentParameters.put("wT", this.wT);
        return experimentParameters;
    }
    
    public void addApplicationToQueue(Application application) {
        this.applicationsQueue.put(application.getJobId(), application);
    }
    
    public Map<String,Application> getApplicationQueue() {
        return this.applicationsQueue;
    }
    
    public Map<Integer,Application> prioritiseApps(Map<String,Application> applications){
        Map<Integer,Application> prioritised = new HashMap<>();
        if(this.approach == 1 || this.approach == 6){
            List list = new LinkedList(applications.entrySet());
            Collections.sort(list,new Comparator() {
                @Override
                public int compare(Object obj1, Object obj2) {
                    try{
                        Map.Entry entry1 = (Map.Entry) obj1;
                        Map.Entry entry2 = (Map.Entry) obj2;
                        Application app1 = (Application) entry1.getValue();
                        Application app2 = (Application) entry2.getValue();
                        if(app1.getPriority()>app2.getPriority())
                            return -1;
                        else if(app1.getPriority()<app2.getPriority())
                            return 1;
                        else if(app1.getPriority()==app2.getPriority()){
                            long now = System.nanoTime();
                            long waitingTimeApp1 = now - app1.getRequestTime();
                            long waitingTimeApp2 = now - app2.getRequestTime();
                            if(waitingTimeApp1==waitingTimeApp2)
                               return 0;
                            else if(waitingTimeApp1>waitingTimeApp2)
                                return -1;
                            else if(waitingTimeApp1<waitingTimeApp2)
                                return 1;   
                        }
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                    return 0;
                }
            });
            for(Iterator it=list.iterator();it.hasNext();){
                Map.Entry entry = (Map.Entry) it.next();
                prioritised.put((prioritised.size()+1), (Application) entry.getValue());
            }
        }
        else{
            Collection<Application> apps = applicationsQueue.values();
            Object [] arrayApps = apps.toArray();
            for(int i=0; i<arrayApps.length; i++){
                prioritised.put((prioritised.size()+1), (Application) arrayApps[i]);
            }
        }
        return prioritised;
    }
    
    public void addFunctionToRemove(Pod function) {
        this.functionsToRemove.put(function.getMetadata().getName(),function);
    }
    
    public Map<String, Application> getScheduledApps() {
        return this.scheduledApps;
    }
    
    private static double[] generateContainerCPUReq(int numberOfContainers) {

        double[] cpuReq = new double[numberOfContainers];
        for (int i = 0; i < numberOfContainers; i++) {
            cpuReq[i] = generateInRange(0.01, 4);
        }
        return cpuReq;
    }

    private static double[] generateContainerRAMReq(int numberOfContainers) {

        double[] ramReq = new double[numberOfContainers];
        for (int i = 0; i < numberOfContainers; i++) {
            ramReq[i] = generateInRange(0.01, 4);
        }
        return ramReq;
    }
    
    private double[][] generateNetworkLatencies(int length) {
        double[][] latencies = new double[length][length];
        for(int i=0; i<length; i++){
            for(int j=0; j<length; j++){
                if(i==j)
                    latencies[i][j] = 0;
                else
                    latencies[i][j] = generateInRange(0.1,5.0);
            }
        }
        return latencies;
    }
    
    private static double generateInRange(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }
    
    private void print (int approach, List<Node> availableNodes, List<Pod> pendingPods, 
            String podName, String nodeName) {
        String ap = "";
        switch(approach){
            case 1:
                ap="MA-ACO";
            break;
            case 2:
                ap="ACO";
            break;
            case 3:
                ap="MaxFit";
            break;
            case 4:
                ap="MultiOpt";
            break;
            case 5:
                ap="Random";
            break;
        }
        
        StringBuilder builder = new StringBuilder();
        builder.append("--- Using " + ap + " algorithm ---\n\n");
        builder.append("Available nodes\n");
        builder.append("---------------\n");
        
        for (Node node : availableNodes) {
            builder.append(node.getMetadata().getName()).append("\n");
        }
        
        builder.append("Pending pods\n");
        builder.append("---------------\n");
        
        for (Pod pod : pendingPods) {
            builder.append(pod.getMetadata().getName()).append("\n");
        }
        
        builder.append("\nPod ");
        builder.append(podName);
        builder.append(" to be scheduled on node ");
        builder.append(nodeName);
        
        System.out.println(builder.toString());
    }
    
    public List<Solution> getResults() {
        return results;
    }

    public void setResults(List<Solution> results) {
        this.results = results;
    }
    
    public OmnetInterface getIface() {
        return iface;
    }

    public void setIface(OmnetInterface iface) {
        this.iface = iface;
    }
}
