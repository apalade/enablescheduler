package ie.scss.tcd.enable.kubernetes.comm;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This interface is used to create the callback when a message arrives from the Omnet++ simulator.
 * 
 */
public interface MessageCallback {
    
    public void callback();
}
