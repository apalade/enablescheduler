package ie.scss.tcd.enable.kubernetes.comm.interfaces;

import ie.scss.tcd.enable.kubernetes.Main;
import ie.scss.tcd.enable.kubernetes.comm.BrokerObserver;
import ie.scss.tcd.enable.kubernetes.comm.MessageBroker;
import ie.scss.tcd.enable.kubernetes.comm.adapters.MQTTAdapter;
import ie.scss.tcd.enable.kubernetes.facades.SchedulerFacade;
import ie.scss.tcd.enable.kubernetes.model.Application;
import ie.scss.tcd.enable.kubernetes.model.Location;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Metadata;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.model.UserType;
import ie.scss.tcd.enable.kubernetes.orchestrator.Orchestrator;
import ie.scss.tcd.enable.kubernetes.util.DataUtils;
import ie.scss.tcd.enable.kubernetes.util.FormatUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the interface with the Omnet++ simulation
 * 
 */
public class OmnetInterface implements BrokerObserver{

    private String TOPIC = "REQUEST";
    private String BROKER = "tcp://192.168.33.4:1883";
    private MessageBroker broker;
    private Main m;
    
    private Map<String,Application> descriptions = new HashMap<>(); 
    private static Map<String,Double> experimentParameters = new HashMap<>();
    private int jobs=0;
    
    private SchedulerFacade facade;
    private Orchestrator controller;
    
    public OmnetInterface(String appsFile, Orchestrator controller, SchedulerFacade facade, Map<String,Double> experimentParameters){
        this.facade = facade;
        this.controller = controller;
        ArrayList<Application> applications = DataUtils.readApplications(appsFile);
        for(int i=0; i<applications.size(); i++){
            Application application = applications.get(i);
            descriptions.put(application.getName(), application);
        }
        this.broker = new MQTTAdapter(BROKER, TOPIC);
        this.experimentParameters = experimentParameters;
    }
    
    public void startListening(Main m){
        this.m=m;
        this.broker.addObserver(this);
        this.broker.connect();
        System.out.println("*** Waiting for Omnet Messages ***");
    }
    
    public Application getDescription(Application app, String jobId, int version, int functions){
        Application newApp = new Application();
        newApp.setJobId(jobId);
        newApp.setName(app.getName() + "-" + jobId);
        newApp.setDone(false);
        newApp.setPriority(app.getPriority());
        if(version>=1){
            version++;
            newApp.setVersion(version);
        }
        else
            newApp.setVersion(1);
            
        for(int i=0; i<app.getPods().size() && i<functions; i++){
            Pod pod = app.getPods().get(i);
            Metadata metadata = new Metadata();
            metadata.setName(app.getName() + "-" + jobId + "-" + pod.getMetadata().getName() + "-v-" + newApp.getVersion());
            metadata.setAnnotations(copyMap(pod.getMetadata().getAnnotations()));
            metadata.getAnnotations().put("application", newApp.getName());
            metadata.setLabels(copyMap(pod.getMetadata().getLabels()));
            Pod newPod = new Pod(metadata);
            newApp.getPods().add(newPod);
        }
        return newApp;
    }
    
    private Map<String, String> copyMap(Map<String, String> oldMap) {
        Map<String,String> newMap = new HashMap<>();
            oldMap.forEach((key,value)->{
                newMap.put(key,value);
            });
        return newMap;
    }
    
    @Override
    public void update(String message) {
        try{
            if(!message.equals(null)){
                Map<String, String> map =  FormatUtils.jsonToObject(message, Map.class);
                String type = map.get("type");
                switch(type){
                    case "StartUp":
                        /*
                            Receving first message with nodes locations and initial network status
                        */
                        List<Node> nodes = controller.getAvailableNodes();
                        double [][] networkLatencies = new double [nodes.size()][nodes.size()];
                        for(int i=0; i<nodes.size();i++){
                            Map<String,String> annotations = new HashMap<>();
                            double l1 = Double.parseDouble(map.get("ubuntu_delay"+(i+2)));
                            for(int j=0; j<nodes.size();j++){
                                if(i==j){
                                    networkLatencies[i][j] = 0.0;
                                    annotations.put("latency"+(j+1), "" + networkLatencies[i][j]);
                                }
                                else{
                                    double l2 = Double.parseDouble(map.get("ubuntu_delay"+(j+2)));
                                    networkLatencies[i][j] = l1+l2;
                                    annotations.put("latency"+(j+1), "" + networkLatencies[i][j]);
                                }
                            }
                            double x = Double.parseDouble(map.get("ubuntu_longitude"+(i+2)));
                            double y = Double.parseDouble(map.get("ubuntu_latitude"+(i+2)));
                            Location location = new Location(x,y);
                            controller.setNodeLocation(nodes.get(i).getMetadata().getName(), location);
                            annotations.put("latitude", "" + y);
                            annotations.put("longitude", "" + x);
                            controller.updateNodeWithLabels(nodes.get(i), annotations);
                        }
                        controller.setNetworkLatencies(networkLatencies);
                        
                        System.out.println("*** Node latencies and locations updated experiment: " + m.getCurrentExperiment()+ " ***");
                    break;
                    case "Link_update":
                        /*
                            Receving updates about the network status
                        */
                        nodes = controller.getAvailableNodes();
                        networkLatencies = new double [nodes.size()][nodes.size()];
                        for(int i=0; i<nodes.size();i++){
                            Map<String,String> annotations = new HashMap<>();
                            double l1 = Double.parseDouble(map.get("ubuntu_delay"+(i+2)));
                            for(int j=0; j<nodes.size();j++){
                                if(i==j){
                                    networkLatencies[i][j] = 0.0;
                                    annotations.put("latency"+(j+1), "" + networkLatencies[i][j]);
                                }
                                else{
                                    double l2 = Double.parseDouble(map.get("ubuntu_delay"+(j+2)));
                                    networkLatencies[i][j] = l1+l2;
                                    annotations.put("latency"+(j+1), "" + networkLatencies[i][j]);
                                }
                            }
                            controller.updateNodeWithLabels(nodes.get(i), annotations);
                        }
                        controller.setNetworkLatencies(networkLatencies);
                        System.out.println("*** Node latencies and locations updated experiment " + m.getCurrentExperiment()+ " ***");
                    break;
                    case "Opera_User_Arrival":
                        /*
                            Receving opera user request and adding app to the queue
                        */
                        Application app = new Application();
                        String jobId = map.get("job_ID");
                        int functions = this.experimentParameters.get("functions").intValue();
                        // Applcation description created from pre-defined applications
                        app = getDescription(descriptions.get("opera"), jobId,0,functions);
                        app.setUserType(UserType.STATIC);
                        app.setRequestTime(System.nanoTime());
                        Double x = Double.parseDouble(map.get("opera_latitude"));
                        Double y = Double.parseDouble(map.get("opera_longitude"));
                        Location location = new Location(x,y);
                        app.setLocation(location);
                        app.setDelayRequirement(Long.parseLong(map.get("opera_delayRequirement")));
                        app.getPods().forEach((pod)->{
                            pod.getMetadata().getAnnotations().put("latitude",""+location.getY());
                            pod.getMetadata().getAnnotations().put("longitude",""+location.getX());
                            pod.getMetadata().getAnnotations().put("delay", map.get("opera_delayRequirement"));
                        });
                        jobs++;
                        app.setJob(jobs);
                        facade.addApplicationToQueue(app);
                        System.out.println("*** Application " + app.getName() + " added to the queue experiment " + m.getCurrentExperiment()+ " ***");
                    break;
                    case "Ambulance_User_Arrival":
                        /*
                            Receving ambulance user request and adding app to the queue
                        */
                        app = new Application();
                        jobId = map.get("job_ID");
                        functions = this.experimentParameters.get("functions").intValue();
                        // Applcation description created from pre-defined applications
                        app = getDescription(descriptions.get("ambulance"), jobId,0,functions);
                        app.setUserType(UserType.MOBILE);
                        app.setRequestTime(System.nanoTime());
                        x = Double.parseDouble(map.get("ambulance_latitude"));
                        y = Double.parseDouble(map.get("ambulance_longitude"));
                        location = new Location(x,y);
                        app.setLocation(location);
                        x = Double.parseDouble(map.get("ambulance_targetHospital_Latitude"));
                        y = Double.parseDouble(map.get("ambulance_targetHospital_Longitude"));
                        Location targetLocation = new Location(x,y);
                        app.setTargetLocation(targetLocation);
                        app.setDelayRequirement(Long.parseLong(map.get("ambulance_delayRequirement")));
                        app.getPods().forEach((pod)->{
                            pod.getMetadata().getAnnotations().put("latitude",""+location.getY());
                            pod.getMetadata().getAnnotations().put("longitude",""+location.getX());
                            pod.getMetadata().getAnnotations().put("tlatitude",""+targetLocation.getY());
                            pod.getMetadata().getAnnotations().put("tlongitude",""+targetLocation.getX());
                            pod.getMetadata().getAnnotations().put("delay", map.get("ambulance_delayRequirement"));
                        });
                        jobs++;
                        app.setJob(jobs);
                        facade.addApplicationToQueue(app);
                        System.out.println("*** Application " + app.getName() + " added to the queue experiment " + m.getCurrentExperiment()+ " ***");
                    break;
                    case "Ambulance_Update":
                        /*
                            Receving ambulance user request updates and adding app to the queue
                        */
                        jobId = map.get("job_ID");
                        Application oldApp = new Application();
                        if(facade.getScheduledApps().containsKey(jobId)){
                            oldApp = facade.getScheduledApps().get(jobId);
                            oldApp.getPods().forEach((function)->{
                                controller.removeFunction(function);
                            });
                        }
                        if(facade.getApplicationQueue().containsKey(jobId)){
                            oldApp = facade.getApplicationQueue().get(jobId);
                            facade.getApplicationQueue().remove(jobId);
                        }
                        
                        String tlatitude = "" + oldApp.getTargetLocation().getY();
                        String tlongitud = "" + oldApp.getTargetLocation().getY();
                        String delay = "" + oldApp.getDelayRequirement();
                            
                        app = new Application();
                        functions = this.experimentParameters.get("functions").intValue();
                        // Applcation description created from pre-defined applications
                        app = getDescription(descriptions.get("ambulance"), jobId,oldApp.getVersion(),functions);
                        app.setUserType(UserType.MOBILE);
                        app.setRequestTime(System.nanoTime());
                        x = Double.parseDouble(map.get("ambulance_latitude"));
                        y = Double.parseDouble(map.get("ambulance_longitude"));
                        location = new Location(x,y);
                        app.setLocation(location);
                        app.setTargetLocation(oldApp.getTargetLocation());
                        app.setDelayRequirement(oldApp.getDelayRequirement());
                        app.getPods().forEach((pod)->{
                            pod.getMetadata().getAnnotations().put("latitude",""+location.getY());
                            pod.getMetadata().getAnnotations().put("longitude",""+location.getX());
                            pod.getMetadata().getAnnotations().put("tlatitude",""+tlatitude);
                            pod.getMetadata().getAnnotations().put("tlongitude",""+tlongitud);
                            pod.getMetadata().getAnnotations().put("delay", ""+delay);
                        });
                        System.out.println("*** Ambulance position updated ***");
                        jobs++;
                        app.setJob(jobs);
                        facade.addApplicationToQueue(app);
                        System.out.println("*** Application " + app.getName() + " added to the queue experiment " + m.getCurrentExperiment()+ " ***");
                    break;
                    case "Ambulance_kill_Process":
                    case "Opera_kill_Process":
                        /*
                            Receving ambulance and opera user end request and removing them
                        */
                        
                        jobId = map.get("job_ID");
                        System.out.println("*** Message arrived to remove " + jobId + " experiment " + m.getCurrentExperiment()+ " ***");
                        app = new Application();
                        if(facade.getScheduledApps().containsKey(jobId)){
                            app = facade.getScheduledApps().get(jobId);
                            facade.getScheduledApps().remove(jobId);
                        }
                        if(facade.getApplicationQueue().containsKey(jobId)){
                            app = facade.getApplicationQueue().get(jobId);
                            facade.getApplicationQueue().remove(jobId);
                        }
                        if(app.getVersion()>1)
                            Thread.sleep(5000);
                        app.getPods().forEach((function)->{
                            controller.removeFunction(function);
                        });
                    break;
                    case "finished":
                        /*
                            Receving message to finish an experiment
                        */
                        m.finish();
                    break;

                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }    

    public void stopListening() {
        broker.connect();
        System.out.println("*** Omnet Listener Stopped ***");
    }

    public void sendExperimentTrigger(String type, Main m) {
        Map<String, String> map =  new HashMap<>();
        map.put("msg","start");
        map.put("type",type);
        String message = FormatUtils.jsonToString(map);
        String t = "EXPERIMENT";
        broker = new MQTTAdapter(BROKER, t);
        broker.connect();
        broker.publishMessage(message);
        broker.disconnect();
        broker = new MQTTAdapter(BROKER, TOPIC);
        startListening(m);
    }

    public void sendPlacementToOmnet(String message) {
        String t = "PLACEMENT";
        broker = new MQTTAdapter(BROKER, t);
        broker.connect();
        broker.publishMessage(message);
        broker.disconnect();
    }
}
