package ie.scss.tcd.enable.kubernetes.comm;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This interface is used to create the MQTT client to send and receive messages to the Omnet++ simulation.
 * 
 */
public interface MessageBroker {
    
    public boolean isConnected();
    
    public void connect();
    
    public void disconnect();
      
    public void publishMessage(String message);
    
    public void addObserver(BrokerObserver o);
    
    public void unregisterObserver(BrokerObserver o);
    
    public void notifyObservers(String message);
}
