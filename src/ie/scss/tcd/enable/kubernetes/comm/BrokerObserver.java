package ie.scss.tcd.enable.kubernetes.comm;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This interface is used to create the MQTT communication with the Omnet++ simulation.
 * 
 */
public interface BrokerObserver {
    
    public void update(String message);
}
