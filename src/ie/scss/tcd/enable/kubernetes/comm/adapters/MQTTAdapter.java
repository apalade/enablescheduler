package ie.scss.tcd.enable.kubernetes.comm.adapters;

import ie.scss.tcd.enable.kubernetes.comm.MessageBroker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import ie.scss.tcd.enable.kubernetes.comm.BrokerObserver;

/**
 * MQTT Adapter
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the the MQTT adapter for receiving and sending messages to the Omnet++ simulator.
 * 
 */
public class MQTTAdapter implements MessageBroker, MqttCallback {
    
    private List<BrokerObserver> observerList;
    
    private final String brokerAddress;
    private final String topic;
    private final int QOS = 2;
    private final String clientId = "KubernetesScheduler";
    
    private MqttClient client;
    
    public MQTTAdapter(String brokerAddress, String topic) {
        this.brokerAddress = brokerAddress;
        this.topic = topic;
        this.observerList = new ArrayList<>();
    }
    
    @Override
    public void connect() {
        
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            client = new MqttClient(brokerAddress, clientId, persistence);
            client.setCallback(this);
            
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.INFO, "Connecting to broker: {0}", brokerAddress);
            client.connect(connOpts);
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.INFO, "Connected");
            client.subscribe(topic);
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.INFO, "Client subscribed to topic: {0}", topic);
            
        } catch (MqttException ex) {
            
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getReasonCode());
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getLocalizedMessage());
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getCause());
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
    }
    
    @Override
    public void publishMessage(String content) {
        
        if (isConnected()) {
            
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(QOS);
            
            try {
                client.publish(topic, message);
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.INFO, "Published message: {0}", content);
                
            } catch (MqttException ex) {
                
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getReasonCode());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getLocalizedMessage());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getCause());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex);
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
            }
        }
    }
    
    @Override
    public void addObserver(BrokerObserver o) {
        observerList.add(o);
    }

    @Override
    public void unregisterObserver(BrokerObserver o) {
        observerList.remove(o);
    }

    @Override
    public void notifyObservers(String message) {
        for (BrokerObserver observer: observerList) {
            observer.update(message);
        }
    }

    @Override
    public boolean isConnected() {
        return client != null && client.isConnected();
    }
    
    @Override
    public void disconnect() {
        
        if (isConnected()) {
            try {
                client.disconnect();
            } catch (MqttException ex) {
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getReasonCode());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getLocalizedMessage());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex.getCause());
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, ex);
                Logger.getLogger(MQTTAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
            }
        }
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
    }

    @Override
    public void messageArrived(String string, MqttMessage mm) throws Exception {
        notifyObservers(new String(mm.getPayload()));
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
    }
}
