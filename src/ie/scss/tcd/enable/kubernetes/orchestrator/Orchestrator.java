package ie.scss.tcd.enable.kubernetes.orchestrator;

import ie.scss.tcd.enable.kubernetes.model.Application;
import ie.scss.tcd.enable.kubernetes.model.Location;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This interface defines the functions that enable the Kubernetes cluster management.
 * 
 */
public interface Orchestrator {

    public void updateNodesWithLabels(List<Node> nodes, Map<String, String> labels);
    
    public void updateNodeWithLabels(Node node, Map<String, String> annotations);
    
    public List<Node> getAvailableNodes();
    
    public List<Pod> getPendingPods();
    
    public Map<String, Map<String, Double>> getNodeUtilisation();
    
    public Map<String, Map<String, Integer>> getAllocatedResources();
    
    public void createFunction(String name);
    
    public void schedule(String nodeName, String podName, Double ram, Double cpu);
    
    public void createWatch();
    
    public void addObserver(OrchestratorObserver observer);
    
    public void notifyObservers(String message);

    public void createFunctions(List<Application> applications);
    
    public void createFunction(Application application);

    public void removeFunctions();
    
    public void removeFunctions(Application application, int type);
    
    public void removeFunction(Pod pod);
    
    public double[][] getNetworkLatencies();

    public void setNetworkLatencies(double[][] networkLatencies);

    public List<Pod> getPendingPods(String name);
    
    public void setNodeLocation(String name, Location location);
 
}
