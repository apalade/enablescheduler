package ie.scss.tcd.enable.kubernetes.orchestrator;

/**
 *
 * @author Andrei Palade <paladea at scss.tcd.ie>
 */
public interface OrchestratorObserver {
    
    public void update(String message);
}
