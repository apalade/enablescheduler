package ie.scss.tcd.enable.kubernetes.orchestrator.adapters;

import ie.scss.tcd.enable.kubernetes.orchestrator.Orchestrator;
import ie.scss.tcd.enable.kubernetes.model.Capacity;
import ie.scss.tcd.enable.kubernetes.model.Metadata;
import ie.scss.tcd.enable.kubernetes.model.Node;
import ie.scss.tcd.enable.kubernetes.model.Pod;
import ie.scss.tcd.enable.kubernetes.model.Status;
import ie.scss.tcd.enable.kubernetes.util.FormatUtils;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.models.V1Binding;
import io.kubernetes.client.models.V1BindingBuilder;
import io.kubernetes.client.models.V1Node;
import io.kubernetes.client.models.V1NodeCondition;
import io.kubernetes.client.models.V1NodeList;
import io.kubernetes.client.models.V1ObjectMeta;
import io.kubernetes.client.models.V1ObjectMetaBuilder;
import io.kubernetes.client.models.V1ObjectReference;
import io.kubernetes.client.models.V1ObjectReferenceBuilder;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.models.V1PodBuilder;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.gson.reflect.TypeToken;
import ie.scss.tcd.enable.kubernetes.model.Application;
import ie.scss.tcd.enable.kubernetes.model.Location;
import ie.scss.tcd.enable.kubernetes.orchestrator.OrchestratorObserver;
import io.kubernetes.client.ProgressRequestBody;
import io.kubernetes.client.ProgressResponseBody;
import io.kubernetes.client.informer.SharedInformerFactory;
import io.kubernetes.client.models.V1Container;
import io.kubernetes.client.models.V1ContainerPort;
import io.kubernetes.client.models.V1DeleteOptions;
import io.kubernetes.client.models.V1Namespace;
import io.kubernetes.client.models.V1NamespaceList;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Andrei Palade <paladea at scss.tcd.ie>
 * This class implements the Orchestrator interface to consume the Kubernetes Java API 
 * 
 */
public class KubernetesAdapter implements Orchestrator {

    /**
     * @return the ramReserved
     */
    public Map<String,Double> getRamReserved() {
        return ramReserved;
    }

    /**
     * @param ramReserved the ramReserved to set
     */
    public void setRamReserved(Map<String,Double> ramReserved) {
        this.ramReserved = ramReserved;
    }

    // Default value for scheduler
    private static final String CUSTOM_SCHEDULER = "customScheduler";
    private static final String MASTER_NODE_LABEL = "node-role.kubernetes.io/master";
    
    // Default labels for Kubernets resources
    private static final String CPU_RESOURCE = "cpu";
    private static final String MEMORY_RESOURCE = "memory";

    // Default values API request
    private static final String NAMESPACE = "default";
    
    private Authenticator auth;
    private SharedInformerFactory factory;
    private List<OrchestratorObserver> listOfObservers;
    
    private double[][] networkLatencies;
    private Map<String,Location> locations = new HashMap<>();
    private Map<String,Double> cpuReserved;
    private Map<String,Double> ramReserved;

    public KubernetesAdapter(String configurationFile) {

        try {
            ApiClient client = Config.fromConfig(configurationFile);
            client.getHttpClient().setReadTimeout(0, TimeUnit.SECONDS); // infinite timeout
            Configuration.setDefaultApiClient(client);
            //client.setDebugging(true);
            CoreV1Api api = new CoreV1Api();
            this.auth = new Authenticator(client, api);
            
        } catch (IOException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        factory = new SharedInformerFactory();
        listOfObservers = new ArrayList<>();
        cpuReserved = new HashMap<>();
        ramReserved = new HashMap<>();
    }

    @Override
    public List<Pod> getPendingPods() {

        List<Pod> listOfPods = new ArrayList<>();

        String namespace = NAMESPACE;
        String pretty = null;
        String _continue = null;
        String fieldSelector = null;
        String labelSelector = null;
        Integer limit = null;
        String resourceVersion = null;
        Integer timeoutSeconds = null;
        Boolean watch = false;

        try {

            V1PodList result = auth.getApi().listNamespacedPod(namespace, pretty, 
                    _continue, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch);

            for (V1Pod item : result.getItems()) {
                if (item.getSpec().getSchedulerName().equals(CUSTOM_SCHEDULER)) {
                    String name = item.getMetadata().getName();
                    Map<String, String> labels = item.getMetadata().getLabels();

                    Metadata metadata = new Metadata(name, null, labels);
                    listOfPods.add(new Pod(metadata));
                }
            }
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }

        return listOfPods;
    }

    @Override
    public void schedule(String nodeName, String podName, Double ram, Double cpu) {
        
        V1ObjectReference target = new V1ObjectReferenceBuilder()
                .withApiVersion("v1")
                .withKind("Node")
                .withNamespace(NAMESPACE)
                .withName(nodeName)
                .build();

        V1ObjectMeta meta = new V1ObjectMetaBuilder()
                .withName(podName)
                .build();

        V1Binding body = new V1BindingBuilder()
                .withMetadata(meta)
                .withTarget(target)
                .build();

        String dryRun = null; // String | When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
        String fieldManager = null; // String | fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
        String pretty = null; // String | If 'true', then the output is pretty printed.

        try {
            V1Binding result = auth.getApi().createNamespacedBinding(NAMESPACE, body, dryRun, fieldManager, pretty);
            double rr = ramReserved.get(nodeName);
            double cr = cpuReserved.get(nodeName);
            rr = rr + ram;
            cr = cr + cpu;
            ramReserved.put(nodeName, rr);
            cpuReserved.put(nodeName, cr);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.INFO, null, result);
        } catch (ApiException ex) {
//            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
//            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
//            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
//            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
    }
    
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    public Watch<V1Pod> podListWatch;
    
    @Override
    public void createWatch() {

        String namespace = NAMESPACE;
        String pretty = null;
        String _continue = null;
        String fieldSelector = null;
        String labelSelector = null;
        Integer limit = null;
        String resourceVersion = null;
        Integer timeoutSeconds = null;
        Boolean watch = true;
        ProgressResponseBody.ProgressListener progresListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        try {
            podListWatch = Watch.createWatch(
                auth.getClient(),
                auth.getApi().listNamespacedPodCall(namespace, pretty, _continue, 
                        fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, 
                        watch, progresListener, progressRequestListener),
                            new TypeToken<Watch.Response<V1Pod>>() {}.getType());
            

            executorService.execute(new WatchHandler());
            
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
    }
    
    @Override
    public void createFunction(Application application) {
        ArrayList<Pod> pods = application.getPods();
        for (int j=0; j<pods.size(); j++){
            Pod p = pods.get(j);
            try{
                V1ContainerPort port = new V1ContainerPort();
                port.setContainerPort(Integer.parseInt(p.getMetadata().getAnnotations().get("container-port")));
                V1Pod pod = new V1PodBuilder()
                    .withNewMetadata()
                    .withName(p.getMetadata().getName())
                    .withAnnotations(p.getMetadata().getAnnotations())
                    .withLabels(p.getMetadata().getLabels())
                    .endMetadata()
                    .withNewSpec()
                    .withSchedulerName(p.getMetadata().getAnnotations().get("scheduler"))
                    .addNewContainer()
                    .withName(p.getMetadata().getAnnotations().get("container-name"))
                    .withImage(p.getMetadata().getAnnotations().get("container-image"))
                    .withPorts(port)
                    .endContainer()
                    .endSpec()
                    .build();
                System.out.println("New POD to create: " + p.getMetadata().getName() + " :: for the application " + p.getMetadata().getAnnotations().get("application"));

                V1Pod result = auth.api.createNamespacedPod("default", pod, null, null, null);
                System.out.println("POD created: " + p.getMetadata().getName());
            }catch(Exception ex){
                //ex.printStackTrace();
            }
        }
    }

    @Override
    public void createFunctions(List<Application> applications) {
        for(int i=0; i<applications.size(); i++){
            Application application = applications.get(i);
            this.createFunction(application);
        }
    }
    
    @Override
    public void removeFunctions(){
        List<Pod> pods = getPendingPods();
        String namespace = NAMESPACE;
        String pretty = "true";
        V1DeleteOptions deleteOptions = new V1DeleteOptions();
        long gracePeriod = 0;
        deleteOptions.setGracePeriodSeconds(gracePeriod);
        String dryRun = "";
        Integer gracePeriodSeconds = 0;
        Boolean orphanDependents = false;                                     
        String propagationPolicy = "";
        
        for(int i=0; i<pods.size();i++){
            Pod pod = pods.get(i);
            try{
                auth.api.deleteNamespacedPod(pod.getMetadata().getName(), namespace, pretty, deleteOptions, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy); 
                Thread.sleep(5000);
            }catch(Exception ex){
            //ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void removeFunctions(Application application, int type) {
        List<Pod> pods = getPendingPods();
        String namespace = NAMESPACE;
        String pretty = "true";
        V1DeleteOptions deleteOptions = new V1DeleteOptions();
        long gracePeriod = 0;
        deleteOptions.setGracePeriodSeconds(gracePeriod);
        String dryRun = "";
        Integer gracePeriodSeconds = null;
        Boolean orphanDependents = false;                                     
        String propagationPolicy = "";
        
        for(int i=0; i<pods.size();i++){
            Boolean remove = false;
            Pod pod = pods.get(i);
            ArrayList<Pod> applicationPods = application.getPods();
            for(int j = 0; j<applicationPods.size(); j++){
                Pod applicationPod = applicationPods.get(j);
                String podPrefix = "";
                String appPodPrefix = "";
                switch(type){
                    case 1:
                        podPrefix = pod.getMetadata().getName().split("-ser")[0];
                        appPodPrefix = applicationPod.getMetadata().getName().split("-ser")[0];
                    break;
                    case 2:
                        podPrefix = pod.getMetadata().getName();
                        appPodPrefix = applicationPod.getMetadata().getName();
                    break;
                }
                if (podPrefix.equals(appPodPrefix)){
                    remove = true;
                    break;
                }
            }
            if(remove){
                try{
                    auth.api.deleteNamespacedPod(pod.getMetadata().getName(), namespace, pretty, deleteOptions, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy); 
                }catch(Exception ex){
                    //ex.printStackTrace();
                }
            }
        }
    }
    
    @Override
    public List<Pod> getPendingPods(String appName) {
        List<Pod> listOfPods = new ArrayList<>();

        String namespace = NAMESPACE;
        String pretty = null;
        String _continue = null;
        String fieldSelector = null;
        String labelSelector = null;
        Integer limit = null;
        String resourceVersion = null;
        Integer timeoutSeconds = null;
        Boolean watch = false;

        try {

            V1PodList result = auth.getApi().listNamespacedPod(namespace, pretty, 
                    _continue, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch);

            for (V1Pod item : result.getItems()) {
                if (item.getSpec().getSchedulerName().equals(CUSTOM_SCHEDULER)
                        && item.getMetadata().getAnnotations().get("application").equals(appName)
                        && !item.getStatus().getPhase().equals("Running")) {
                    String name = item.getMetadata().getName();
                    Map<String, String> labels = item.getMetadata().getLabels();
                    Map<String, String> annotations = item.getMetadata().getAnnotations();

                    Metadata metadata = new Metadata(name, annotations, labels);
                    listOfPods.add(new Pod(metadata));
                }
            }
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }

        return listOfPods;
    }

    @Override
    public void setNodeLocation(String name, Location location) {
        this.locations.put(name, location);
    }

    @Override
    public void removeFunction(Pod pod) {
        String namespace = NAMESPACE;
        String pretty = "true";
        V1DeleteOptions deleteOptions = new V1DeleteOptions();
        long gracePeriod = 0;
        deleteOptions.setGracePeriodSeconds(gracePeriod);
        String dryRun = "";
        Integer gracePeriodSeconds = null;
        Boolean orphanDependents = false;                                     
        String propagationPolicy = "";
        
        try{
            String n = pod.getNode();
            if(n!=null){
                double ram = Double.parseDouble(pod.getMetadata().getAnnotations().get("ram"));
                double cpu = Double.parseDouble(pod.getMetadata().getAnnotations().get("cpu"));
                double rr = ramReserved.get(n);
                double cr = cpuReserved.get(n);
                rr = rr - ram;
                cr = cr - cpu;
                ramReserved.put(n, rr);
                cpuReserved.put(n, cr);
            }
            auth.api.deleteNamespacedPod(pod.getMetadata().getName(), namespace, pretty, deleteOptions, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy); 
        }catch(Exception ex){
            //ex.printStackTrace();
        }
    }

    public class WatchHandler implements Runnable {
        @Override
        public void run() {
            
            while (true) {
                
                for (Watch.Response<V1Pod> item : podListWatch) {
                    if (item.object != null
                            && item.object.getMetadata().getName().equalsIgnoreCase("yolo-function")) {

                        //System.out.println(item.object.getStatus());
                        String phase = item.object.getStatus().getPhase();
                        notifyObservers(phase);
                    }
                }

                try {
                    podListWatch.close();
                } catch (IOException ex) {
                    Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public List<String> getAllNamespaces() {
        
        List<String> listOfNamespaces = new ArrayList<>();
        
        String pretty = null;
        String _continue = null;
        String fieldSelector = null;
        String labelSelector = null;
        Integer limit = null;
        String resourceVersion = null;
        Integer timeoutSeconds = null;
        Boolean watch = false;
        
        try {
            V1NamespaceList result = auth.getApi().listNamespace(pretty, _continue, fieldSelector, 
                    labelSelector, limit, resourceVersion, timeoutSeconds, watch);
            
            for (V1Namespace item: result.getItems()) {
                listOfNamespaces.add(item.getMetadata().getName());
            }
            
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfNamespaces;
    }
    
    @Override
    public Map<String, Map<String, Double>> getNodeUtilisation() {
        
        Map<String, Map<String, Double>> mapOfNodeUtilisation = new HashMap<>();
        Map<String, Map<String, Integer>> mapOfAllocatedResources = getAllocatedResources();
        List<V1Node> listOfNodes = getAllNodes();
        
        for (V1Node node: listOfNodes) {
            
            Map<String, Double> mapOfUtilisation = new HashMap<>();
            String nodeName = node.getMetadata().getName();
            
            for (Map.Entry<String, Integer> resource: mapOfAllocatedResources.get(nodeName).entrySet()) {

                if (resource.getKey().equals("cpu")) {
                    
                    // Calculates the utilisation percentage of the CPU
                    // CPU resources are measured in millicore, where m stands for “thousandth of a core.”
                    // 1000m or 1000 millicore is equal to 1 core. 4000m would represent 4 cores. 
                    // 250 millicore per pod means 4 pods with a similar value of 250m can run on a single core. 
                    // On a 4 core node, 16 pods each having 250m can run on that node.
                    Quantity cpu = node.getStatus().getCapacity().get(CPU_RESOURCE);
                    int cores = cpu.getNumber().intValue();
                    int allocated = resource.getValue();
                    double value = ((double) allocated / (cores * 1000)) * 100;
                    mapOfUtilisation.put("cpu", Double.parseDouble(new DecimalFormat("##.##").format(value)));
                }
                else if (resource.getKey().equals("ram")) {
                   
                    // Calculates the utilisation percentage of the ramp
                    Quantity ram = node.getStatus().getCapacity().get(MEMORY_RESOURCE);
                    int allocated = resource.getValue();
                    double value = new BigDecimal(allocated).divide(ram.getNumber(), 4, RoundingMode.CEILING).doubleValue() * 100;
                    mapOfUtilisation.put("ram", Double.parseDouble(new DecimalFormat("##.##").format(value)));
                }
                else {
                    // TODO add formulas to calculate utilisation for other metrics
                }
            }
            
            mapOfNodeUtilisation.put(nodeName, mapOfUtilisation);
        }

        return mapOfNodeUtilisation;
    }
    
    private List<V1Node> getAllNodes() {
        
        String pretty = null; // String | If 'true', then the output is pretty printed.
        String _continue = null; // String | The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \"next key\".  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
        String fieldSelector = null; // String | A selector to restrict the list of returned objects by their fields. Defaults to everything.
        String labelSelector = null; // String | A selector to restrict the list of returned objects by their labels. Defaults to everything.
        Integer limit = null; // Integer | limit is a maximum number of responses to return for a list call. If more items exist, the server will set the `continue` field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
        String resourceVersion = null; // String | When specified with a watch call, shows changes that occur after that particular version of a resource. Defaults to changes from the beginning of history. When specified for list: - if unset, then the result is returned from remote storage based on quorum-read flag; - if it's 0, then we simply return what we currently have in cache, no guarantee; - if set to non zero, then the result is at least as fresh as given rv.
        Integer timeoutSeconds = null; // Integer | Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
        Boolean watch = false; // Boolean | Watch for changes to the described resources and return them as a stream of add, update, and remove notifications. Specify resourceVersion.

        try {

            V1NodeList result = auth.getApi().listNode(pretty, _continue, fieldSelector, labelSelector, limit, 
                    resourceVersion, timeoutSeconds, watch);
            
            return result.getItems();
            
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
        
        return new ArrayList<>();
    }
    
    
    @Override
    public Map<String, Map<String, Integer>> getAllocatedResources() {

        Map<String, Map<String, Integer>> mapOfAllocatedResoures = new HashMap<>();
        
        String pretty = null;
        String _continue = null;
        String fieldSelector = null;
        String labelSelector = null;
        Integer limit = null;
        String resourceVersion = null;
        Integer timeoutSeconds = null;
        Boolean watch = false;

        try {           
            List<String> listOfNamespaces = getAllNamespaces();
            List<V1Pod> listOfPods = new ArrayList<>();
            
            for (String namespace: listOfNamespaces) {
                
                V1PodList result = auth.getApi().listNamespacedPod(namespace, pretty, _continue, fieldSelector, 
                        labelSelector, limit, resourceVersion, timeoutSeconds, watch);
            
                listOfPods.addAll(result.getItems());
            }
            
            for (V1Pod pod: listOfPods) {

                List<V1Container> listOfContainers = pod.getSpec().getContainers();
                String node = pod.getSpec().getNodeName();
                
                for (V1Container container: listOfContainers) {
                    
                    if (container.getResources().getRequests() != null) {
                        
                        if (!mapOfAllocatedResoures.containsKey(node)) {
                            Map<String, Integer> resource = new HashMap<>();
                            resource.put("ram", 0);
                            resource.put("cpu", 0);
                            mapOfAllocatedResoures.put(node, resource);
                        }
                        
                        Quantity cpuQuantity = container.getResources().getRequests().get(CPU_RESOURCE);
                        
                        if (cpuQuantity != null) {
                            int cpuValue = cpuQuantity.getNumber().unscaledValue().intValue();
                            mapOfAllocatedResoures.get(node).put("cpu", mapOfAllocatedResoures.get(node).get(CPU_RESOURCE) + cpuValue);
                        }
                        
                        Quantity ramQuantity = container.getResources().getRequests().get(MEMORY_RESOURCE);
                        
                        if (ramQuantity != null) {
                            int ramValue = ramQuantity.getNumber().unscaledValue().intValue();
                            mapOfAllocatedResoures.get(node).put("ram", mapOfAllocatedResoures.get(node).get("ram") + ramValue);
                        }
                    } 
                }
            }
            
            for (Map.Entry<String, Map<String, Integer>> item: mapOfAllocatedResoures.entrySet()) {
                System.out.println(item.getKey() + " -> cpu: " + item.getValue().get("cpu") + " ram: " + item.getValue().get("ram") );                
            }
            
        } catch (Exception ex) {
            /*Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));*/
        }

        return mapOfAllocatedResoures;
    }
    
    @Override
    public List<Node> getAvailableNodes() {

        List<Node> listOfNodes = new ArrayList<>();

        String pretty = null; // String | If 'true', then the output is pretty printed.
        String _continue = null; // String | The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \"next key\".  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
        String fieldSelector = null; // String | A selector to restrict the list of returned objects by their fields. Defaults to everything.
        String labelSelector = null; // String | A selector to restrict the list of returned objects by their labels. Defaults to everything.
        Integer limit = null; // Integer | limit is a maximum number of responses to return for a list call. If more items exist, the server will set the `continue` field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
        String resourceVersion = null; // String | When specified with a watch call, shows changes that occur after that particular version of a resource. Defaults to changes from the beginning of history. When specified for list: - if unset, then the result is returned from remote storage based on quorum-read flag; - if it's 0, then we simply return what we currently have in cache, no guarantee; - if set to non zero, then the result is at least as fresh as given rv.
        Integer timeoutSeconds = null; // Integer | Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
        Boolean watch = false; // Boolean | Watch for changes to the described resources and return them as a stream of add, update, and remove notifications. Specify resourceVersion.

        try {

            V1NodeList result = auth.getApi().listNode(pretty, _continue,
                    fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch);

            for (V1Node item : result.getItems()) {

                List<V1NodeCondition> listOfConditions = item.getStatus().getConditions();

                for (V1NodeCondition condition : listOfConditions) {

                    if (condition.getType().equals(Conditions.READY) && condition.getStatus().equals(Conditions.TRUE)) {

                        String name = item.getMetadata().getName();
                        Map<String, String> labelsMap = item.getMetadata().getLabels();
                        Map<String, String> annotationsMap = item.getMetadata().getAnnotations();

                        Map<String, Capacity> capacityMap = new HashMap<>();
                        Map<String, Capacity> allocatableMap = new HashMap<>();

                        populateCapacityMap(item.getStatus().getCapacity(), capacityMap);
                        populateCapacityMap(item.getStatus().getAllocatable(), allocatableMap);

                        Metadata metadata = new Metadata(name, annotationsMap, labelsMap);
                        Status status = new Status(capacityMap, allocatableMap);

                        Node node = new Node(metadata, status);
                        
                        if(cpuReserved.containsKey(name)){
                            double cpuRes = cpuReserved.get(name);
                            node.setCpuReserved(cpuRes);
                        }else{
                            cpuReserved.put(name, 0.0);
                            node.setCpuReserved(0.0);
                        }
                        
                        if(ramReserved.containsKey(name)){
                            double ramRes = ramReserved.get(name);
                            node.setRamReserved(ramRes);
                        }else{
                            ramReserved.put(name, 0.0);
                            node.setRamReserved(0.0);
                        }
                        
                        double availableRAM = item.getStatus().getAllocatable().get(MEMORY_RESOURCE).getNumber().doubleValue();
                        availableRAM = availableRAM/1024/1024/1024;
                        double totalRAM = item.getStatus().getCapacity().get(MEMORY_RESOURCE).getNumber().doubleValue();
                        totalRAM = totalRAM/1024/1024/1024;
                        node.setAvailableCpu(item.getStatus().getAllocatable().get(CPU_RESOURCE).getNumber().doubleValue());
                        node.setAvailableRam(availableRAM);
                        node.setCpuTotal(item.getStatus().getCapacity().get(CPU_RESOURCE).getNumber().doubleValue());
                        node.setRamTotal(totalRAM);
                        
                        // Only worker nodes are added to the list
                        if (!labelsMap.containsKey(MASTER_NODE_LABEL)) {
                            node.setType(Node.Type.WORKER);
                            node.setLocation(this.locations.get(node.getMetadata().getName()));
                            if(node.getRamReserved()<node.getRamTotal() && node.getCpuReserved()<node.getCpuTotal())
                                listOfNodes.add(node);
                        }
                    }
                }
            }

        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
        return listOfNodes;
    }

    @Override
    public void updateNodesWithLabels(List<Node> nodes, Map<String, String> values) {
        
        String pretty = null; // String | If 'true', then the output is pretty printed.
        String dryRun = null; // String | When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
        String fieldManager = null;  // String | fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint. This field is required for apply requests (application/apply-patch) but optional for non-apply patch types (JsonPatch, MergePatch, StrategicMergePatch).
        Boolean force = null; // Boolean | Force is going to \"force\" Apply requests. It means user will re-acquire conflicting fields owned by other people. Force flag must be unset for non-apply patch requests.
        
        try {
            
            for (Node node: nodes) {
                
                String name = node.getMetadata().getName();
                
                String value = values.get(name);
                V1Patch body = new V1Patch(createOperation(Labels.Latitude,value));
                auth.getApi().patchNode(name, body, pretty, dryRun, fieldManager, force);
            }

        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
    }
    
    @Override
    public void updateNodeWithLabels(Node node, Map<String, String> annotations) {
        String pretty = null; // String | If 'true', then the output is pretty printed.
        String dryRun = null; // String | When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
        String fieldManager = null;  // String | fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint. This field is required for apply requests (application/apply-patch) but optional for non-apply patch types (JsonPatch, MergePatch, StrategicMergePatch).
        Boolean force = null; // Boolean | Force is going to \"force\" Apply requests. It means user will re-acquire conflicting fields owned by other people. Force flag must be unset for non-apply patch requests.
        
        try {
            if(annotations.containsKey("latitude")){
                V1Patch body = new V1Patch(createOperation(Labels.Latitude,annotations.get("latitude")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("longitude")){
                V1Patch body = new V1Patch(createOperation(Labels.Longitude,annotations.get("longitude")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("latency1")){
                V1Patch body = new V1Patch(createOperation(Labels.Latency1,annotations.get("latency1")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("latency2")){
                V1Patch body = new V1Patch(createOperation(Labels.Latency2,annotations.get("latency2")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("latency3")){
                V1Patch body = new V1Patch(createOperation(Labels.Latency3,annotations.get("latency3")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("latency4")){
                V1Patch body = new V1Patch(createOperation(Labels.Latency4,annotations.get("latency4")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            if(annotations.containsKey("pods")){
                V1Patch body = new V1Patch(createOperation(Labels.Pods,annotations.get("pods")));
                auth.getApi().patchNode(node.getMetadata().getName(), body, pretty, dryRun, fieldManager, force);
            }
            
            
        } catch (ApiException ex) {
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getCode());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, ex.getResponseBody());
            Logger.getLogger(KubernetesAdapter.class.getName()).log(Level.SEVERE, null, Arrays.toString(ex.getStackTrace()));
        }
    }
    
    /**
     * Copy the quantity map from the Kubernetes API to this capacity map
     *
     * @param quantityList
     * @param capacityList
     */
    void populateCapacityMap(Map<String, Quantity> quantityList, Map<String, Capacity> capacityList) {

        for (Map.Entry<String, Quantity> quantity : quantityList.entrySet()) {
            Quantity.Format format = quantity.getValue().getFormat();
            Capacity capacity = new Capacity(quantity.getValue().getNumber(), quantityToCapacity(format));
            capacityList.put(quantity.getKey(), capacity);
        }
    }

    /**
     * Copies from one format to the other
     * 
     * @param qFormat
     * @return 
     */
    static Capacity.Format quantityToCapacity(Quantity.Format qFormat) {
        return Capacity.Format.values()[qFormat.ordinal()];
    }

    @Override
    public void createFunction(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This is the class to authenticate the API controller
     */
    static final class Authenticator {

        public ApiClient client;
        public CoreV1Api api;

        public Authenticator(ApiClient client, CoreV1Api api) {
            this.client = client;
            this.api = api;
        }

        public ApiClient getClient() {
            return client;
        }

        public CoreV1Api getApi() {
            return api;
        }
    }
    
    /**
     * These are conditions of the Kubernetes cluster
     */
    static final class Conditions {
 
        public static final String READY = "Ready";
        public static final String TRUE = "True";
    }
    
    /**
     * These are operations types in Kubernetes cluster
     */
    static final class Operations {
        
        public static final String REPLACE = "replace";
    }
    
    /**
     * These are the paths/labels that need to be update
     */
    static final class Labels {
        
        public static final String RTT = "/metadata/labels/RTT";
        public static final String Latitude = "/metadata/annotations/latitude";
        public static final String Longitude = "/metadata/annotations/longitude";
        public static final String Latency1 = "/metadata/annotations/latency1";
        public static final String Latency2 = "/metadata/annotations/latency2";
        public static final String Latency3 = "/metadata/annotations/latency3";
        public static final String Latency4 = "/metadata/annotations/latency4";
        public static final String Latency = "/metadata/annotations/latency";
        public static final String Distance = "/metadata/annotations/distance";
        public static final String Pods = "/metadata/annotations/pods";
    }
    
    /**
     * Class to create a path operation in Kubernetes
     */
    class Operation {
        
        private final String op;
        private final String path;
        private final String value;
        
        public Operation(String name, String path, String value) {
            this.op = name;
            this.path = path;
            this.value = value;
        }
    }
    
    /**
     * Creates the JSON for a Kubernetes Operation
     * 
     * @param value The value to be added to the operation
     * @return A JSON string of the operation
     */
    private String createOperation(String property, String value) {
        List<Operation> list = new ArrayList<>();
        list.add(new Operation(Operations.REPLACE, property, value));
        return FormatUtils.jsonToString(list);
    }
    
    @Override
    public void addObserver(OrchestratorObserver observer) {
        listOfObservers.add(observer);
    }
    
    @Override
    public void notifyObservers(String message) {
        for (OrchestratorObserver observer: listOfObservers) {
            observer.update(message);
        }
    }
    
    @Override
    public double[][] getNetworkLatencies() {
        return networkLatencies;
    }

    @Override
    public void setNetworkLatencies(double[][] networkLatencies) {
        this.networkLatencies = networkLatencies;
    }
    
    public Map<String,Double> getCpuReserved() {
        return cpuReserved;
    }

    public void setCpuReserved(Map<String,Double> cpuReserved) {
        this.cpuReserved = cpuReserved;
    }
}
